package ru.justgo.fitcolors;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import ru.justgo.fitcolors.factories.FontFactory;
import ru.justgo.fitcolors.screens.BaseScreen;
import ru.justgo.fitcolors.screens.main.MainScreen;
import ru.justgo.fitcolors.services.Services;
import ru.justgo.fitcolors.services.ifaces.GoogleAccountService;
import ru.justgo.fitcolors.services.ifaces.NetworkService;
import ru.justgo.fitcolors.services.ifaces.ScreenSwitcherService;


public class FitColors extends ApplicationAdapter {
    private BaseScreen currentScreen;

    @Override
    public void create() {
        System.out.println("FitColors#create");

        FontFactory.initializeFonts();

        Services.initServices(this);
        Services.getService(GoogleAccountService.class).login();
        Services.getService(NetworkService.class).registerCurrentUser();

//		((GameConfigServiceImpl)Services.getService(GameConfigService.class)).setCurrentGameType(GameType.SINGLE_SWIPE);
        Services.getService(ScreenSwitcherService.class).switchScreen(MainScreen.class);
    }

    @Override
    public void render() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        currentScreen.render(Gdx.graphics.getDeltaTime());
    }

    public void setCurrentScreen(BaseScreen currentScreen) {
        this.currentScreen = currentScreen;
    }

    public BaseScreen getCurrentScreen() {
        return currentScreen;
    }
}
