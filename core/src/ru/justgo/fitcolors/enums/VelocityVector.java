package ru.justgo.fitcolors.enums;

import com.badlogic.gdx.math.Vector2;

public enum VelocityVector {
    LEFT(-1, 0), RIGHT(1, 0), UP(0, 1), DOWN(0, -1);
    // ****************************************************
    private Vector2 velocity;

    VelocityVector(float vx, float vy) {
        velocity = new Vector2(vx, vy);
    }

    public Vector2 getVelocity() {
        return velocity.cpy();
    }
}
