package ru.justgo.fitcolors.enums;

public enum GameType
{
	SINGLE_SWIPE(1), MULTI_SWIPE(2), TOUCH(3);

	private int code;

	GameType(int code)
	{
		this.code = code;
	}

	public int getCode()
	{
		return code;
	}

	public static GameType findByCode(int code)
	{
		for (GameType gameType : GameType.values())
		{
			if (gameType.code == code)
			{
				return gameType;
			}
		}
		throw new IllegalArgumentException(String.format("Can't find game type with code: [%s]", code));
	}
}
