package ru.justgo.fitcolors.enums;

public enum ObstacleOrientation
{
	HORIZONTAL, VERTICAL
}
