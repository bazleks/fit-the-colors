package ru.justgo.fitcolors.enums;

public enum ColorChooserDirection {
    TO_LEFT, TO_RIGHT, TO_UP, TO_DOWN
}
