package ru.justgo.fitcolors.utils;

import com.badlogic.gdx.graphics.Color;

/**
 * @author bazleks on 25.07.2015.
 */
public class FCConstants {
    public static Color mainScreenColor = Color.valueOf("f3efe4");
    public static Color brownColor = Color.valueOf("5f554e");
    public static Color recordUsualColor = Color.valueOf("778ebc");
    public static Color recordUsualColorTransparent = Color.valueOf("778ebc").sub(0, 0, 0, 0.5f);
    public static Color transparentBlack = Color.BLACK.cpy().sub(0f, 0f, 0f, 0.5f);
    public static Color recordBestColor = Color.valueOf("f4bf62");

}
