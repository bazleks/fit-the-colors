package ru.justgo.fitcolors.utils.collisions;

import ru.justgo.fitcolors.common.ui.actors.Obstacle;
import ru.justgo.fitcolors.common.ui.actors.Square;

public interface CollisionListener {
    void onSquareHitObstacle(Square square, Obstacle obstacle);
}
