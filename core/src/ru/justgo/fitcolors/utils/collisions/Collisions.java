package ru.justgo.fitcolors.utils.collisions;

import com.badlogic.gdx.math.Rectangle;
import ru.justgo.fitcolors.common.ui.SquareTexture;
import ru.justgo.fitcolors.common.ui.actors.Obstacle;
import ru.justgo.fitcolors.common.ui.actors.Square;

import java.util.List;

import static com.badlogic.gdx.math.Intersector.intersectRectangles;

public class Collisions {

    private static Rectangle rect = new Rectangle();
    private static Rectangle obst = new Rectangle();

    public static void checkForCollisions(List<Obstacle> obstacles, List<Square> squares, CollisionListener collisionListener) {
        for (Obstacle obstacle : obstacles) {
            for (Square square : squares) {
                if (squareCrossObstacle(square, obstacle)) {
                    collisionListener.onSquareHitObstacle(square, obstacle);
                }
            }
        }
    }

    private static boolean squareCrossObstacle(SquareTexture actor, Obstacle obstacle) {
        rect.set(actor.getX(), actor.getY(), actor.getWidth(), actor.getHeight());
        obst.set(obstacle.getX(), obstacle.getY(), obstacle.getWidth(), obstacle.getHeight());
        return intersectRectangles(rect, obst, new Rectangle());
    }


}
