package ru.justgo.fitcolors.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;

import ru.justgo.fitcolors.common.ui.ShadowedLabel;
import ru.justgo.fitcolors.common.ui.ShadowedSquare;
import ru.justgo.fitcolors.common.ui.SquareTexture;


public class FCUtils
{
	private FCUtils()
	{
	}

	public static TextureRegion createWhiteTexture(float w, float h, float alpha)
	{
		Pixmap pixmap = new Pixmap(1, 1, Pixmap.Format.RGBA8888);
		pixmap.setColor(1f, 1f, 1f, alpha);
		pixmap.fill();
		Texture texture = new Texture(pixmap);
		pixmap.dispose();
		return new TextureRegion(texture, (int) w, (int) h);
	}

	public static Texture createWhiteTexture()
	{
		Pixmap pixmap = new Pixmap(1, 1, Pixmap.Format.RGBA8888);
		pixmap.setColor(Color.WHITE);
		pixmap.fill();
		Texture texture = new Texture(pixmap);
		pixmap.dispose();
		return texture;
	}

	final static Color shadowColor = Color.BLACK.cpy().sub(0f, 0f, 0f, 0.5f);


	public static ShadowedSquare createShadowedSquare(float x, float y, float size)
	{
		ShadowedSquare shadowedSquare = new ShadowedSquare();
		shadowedSquare.setBounds(x, y, size, size);

		return shadowedSquare;
	}

	public static SquareTexture createShadowTexture(float x, float y, float size)
	{
		SquareTexture squareTexture = new SquareTexture();
		squareTexture.setBounds(x, y, size * 3, size);

		squareTexture.setColor(shadowColor);
		squareTexture.rotateBy(-45);
		return squareTexture;
	}

	public static BitmapFont createFont(Color color, int size)
	{
		FreeTypeFontGenerator freeTypeFontGenerator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/onramp.ttf"));
		FreeTypeFontGenerator.FreeTypeFontParameter parameters = new FreeTypeFontGenerator.FreeTypeFontParameter();
		parameters.size = size;
		parameters.color = color;
		final BitmapFont bitmapFont = freeTypeFontGenerator.generateFont(parameters);
		freeTypeFontGenerator.dispose();
		return bitmapFont;
	}

	public static BitmapFont createShadowedFont(Color color, int size)
	{
		FreeTypeFontGenerator freeTypeFontGenerator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/onramp.ttf"));
		FreeTypeFontGenerator.FreeTypeFontParameter parameters = new FreeTypeFontGenerator.FreeTypeFontParameter();
		parameters.shadowColor = Color.BLACK.cpy().sub(0, 0, 0, 0.5f);
		parameters.shadowOffsetX = 2;
		parameters.shadowOffsetY = 2;
		parameters.size = size;
		parameters.color = color;
		final BitmapFont bitmapFont = freeTypeFontGenerator.generateFont(parameters);
		freeTypeFontGenerator.dispose();
		return bitmapFont;
	}

	public static Label createLabel(BitmapFont bitmapFont, String text)
	{
		Label.LabelStyle labelStyle = new Label.LabelStyle();
		labelStyle.font = bitmapFont;
		final Label label = new Label(text, labelStyle);
		label.setAlignment(Align.center);
		return label;
	}

	public static Label createLabel(BitmapFont bitmapFont)
	{
		return createLabel(bitmapFont, " ");
	}

	public static Image createImageFromTexture(String path)
	{
		Texture texture = new Texture(Gdx.files.internal(path));
		TextureRegion textureRegion = new TextureRegion(texture);
		final Image image = new Image(textureRegion);
		image.setScaling(Scaling.fit);
		return image;
	}

	public static Image createImageFromTexture(String path, int width, int height)
	{
		Pixmap originPixmap = new Pixmap(Gdx.files.internal(path));
		Pixmap resizedPixmap = new Pixmap(width, height, originPixmap.getFormat());
		resizedPixmap.drawPixmap(originPixmap, 0, 0, originPixmap.getWidth(), originPixmap.getHeight(), 0, 0, width, height);

		Texture texture = new Texture(resizedPixmap);
		final Image image = new Image(texture);
		image.setScaling(Scaling.fit);
		originPixmap.dispose();
		resizedPixmap.dispose();

		return image;
	}

	public static <T> T getRandFromArray(T[] array)
	{
		int length = array.length;
		int i = MathUtils.random(0, length - 1);
		return (T) array[i];
	}

	public static <T> T getRandFromEnum(Class<T> enumClass)
	{
		if (enumClass.isEnum())
		{
			return getRandFromArray(enumClass.getEnumConstants());
		}
		throw new IllegalArgumentException(String.format("[%s] is not enum!", enumClass));
	}

	public static ShadowedLabel createShadowedLabel(BitmapFont bitmapFont, String text)
	{
		return new ShadowedLabel(bitmapFont, text);
	}

	public static ShadowedLabel createShadowedLabel(BitmapFont bitmapFont)
	{
		return new ShadowedLabel(bitmapFont, " ");
	}

	public static void sleep(int millis)
	{
		try
		{
			Thread.sleep(millis);
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
	}
}
