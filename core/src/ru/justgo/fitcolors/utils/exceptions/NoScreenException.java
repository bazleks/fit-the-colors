package ru.justgo.fitcolors.utils.exceptions;

public class NoScreenException extends RuntimeException
{
	public NoScreenException(Throwable cause)
	{
		super(cause);
	}
}
