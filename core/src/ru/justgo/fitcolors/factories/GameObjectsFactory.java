package ru.justgo.fitcolors.factories;


import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;
import static ru.justgo.fitcolors.enums.ColorType.*;
import static ru.justgo.fitcolors.enums.ObstacleOrientation.HORIZONTAL;
import static ru.justgo.fitcolors.enums.ObstacleOrientation.VERTICAL;

import java.util.Arrays;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.utils.Align;

import ru.justgo.fitcolors.common.ui.actors.Obstacle;
import ru.justgo.fitcolors.common.ui.actors.Square;
import ru.justgo.fitcolors.common.ui.actors.squaresArea.SquaresArea;
import ru.justgo.fitcolors.enums.ColorType;
import ru.justgo.fitcolors.enums.ObstacleOrientation;
import ru.justgo.fitcolors.enums.VelocityVector;
import ru.justgo.fitcolors.screens.game.GameSquaresArea;
import ru.justgo.fitcolors.services.Services;
import ru.justgo.fitcolors.services.ifaces.GameScoreService;
import ru.justgo.fitcolors.services.ifaces.ObjectsSizeCalculatorService;
import ru.justgo.fitcolors.services.ifaces.SquareSpeedService;
import ru.justgo.fitcolors.utils.FCUtils;


public class GameObjectsFactory
{

	public static final float OBSTACLE_SIZE = 15f;
	public static final int SIZE_MAX = 150;
	public static final int SIZE_ZERO = 0;
	public static final float APPEARANCE_DURATION = 1f;

	private GameObjectsFactory()
	{
	}

	public static Square createSquare(GameSquaresArea gameSquaresArea)
	{
		ColorType randomColorType = FCUtils.getRandFromArray(ColorType.values());
		Square square = new Square();
		square.setColorType(randomColorType);
		float squareSize = Services.getService(ObjectsSizeCalculatorService.class).getSquareSize();
		square.setSize(squareSize, squareSize);
		square.setOrigin(Align.center);
		square.getColor().a = 0f;
		square.addAction(Actions.alpha(1f, 1f));

		VelocityVector velocityVector = FCUtils.getRandFromArray(VelocityVector.values());
		float x = 0f, y = 0f;
		float obstacleSize = Services.getService(ObjectsSizeCalculatorService.class).getObstacleSize();
		square.getColor().a = 0f;

		switch (velocityVector)
		{
			case LEFT:
				x = Gdx.graphics.getWidth();
				y = MathUtils.random(obstacleSize, gameSquaresArea.getHeight() - obstacleSize - square.getHeight());
				break;
			case RIGHT:
				x = -square.getWidth();
				y = MathUtils.random(obstacleSize, gameSquaresArea.getHeight() - obstacleSize - square.getHeight());
				break;
			case UP:
				x = MathUtils.random(obstacleSize, gameSquaresArea.getWidth() - obstacleSize - square.getWidth());
				y = -square.getHeight();
				break;
			case DOWN:
				x = MathUtils.random(obstacleSize, gameSquaresArea.getWidth() - obstacleSize - square.getWidth());
				y = Gdx.graphics.getHeight();
				break;
		}
		square.setPosition(x, y);
		int score = Services.getService(GameScoreService.class).getTotalScore();
		float v = Services.getService(SquareSpeedService.class).squareSpeed(score);

		square.setVelocity(velocityVector.getVelocity().cpy().scl(v));

		return square;
	}

	/**
	 * Create new Actor for Obstacle which should be disappeared after selected time in {@param timePeriod}
	 *
	 * @param timePeriod
	 * 		- time to disappear in seconds
	 * @return - new Actor of Obstacle
	 */
	public static Obstacle createObstacle(float timePeriod, SquaresArea squaresArea)
	{
		float widthStarted;
		float widthFinished;
		float heightStarted;
		float heightFinished;
		ColorType randomColorType = FCUtils.getRandFromEnum(ColorType.class);
		Obstacle obstacle = new Obstacle(randomColorType);
		obstacle.getColor().a = 0f;
		ObstacleOrientation randomObstacleOrientation = FCUtils.getRandFromEnum(ObstacleOrientation.class);
		if (randomObstacleOrientation == ObstacleOrientation.HORIZONTAL)
		{
			widthStarted = SIZE_ZERO;
			heightStarted = OBSTACLE_SIZE;
			widthFinished = SIZE_MAX;
			heightFinished = OBSTACLE_SIZE;
		}
		else if (randomObstacleOrientation == ObstacleOrientation.VERTICAL)
		{
			widthStarted = OBSTACLE_SIZE;
			heightStarted = SIZE_ZERO;
			widthFinished = OBSTACLE_SIZE;
			heightFinished = SIZE_MAX;
		}
		else
		{
			throw new IllegalArgumentException(String.format("Implementation for [%s] is not defined", randomObstacleOrientation));
		}
		obstacle.setSize(widthStarted, heightStarted);
		obstacle.addAction(Actions.parallel(Actions.alpha(1f, APPEARANCE_DURATION),
				Actions.sizeTo(widthFinished, heightFinished, APPEARANCE_DURATION, Interpolation.exp5)));

		float x = MathUtils.random(OBSTACLE_SIZE, squaresArea.getWidth() - widthFinished - OBSTACLE_SIZE);
		float y = MathUtils.random(OBSTACLE_SIZE, squaresArea.getHeight() - heightFinished - OBSTACLE_SIZE);
		obstacle.setPosition(x, y);

		obstacle.addAction(sequence(delay(timePeriod), alpha(0f, 1f), hide()));

		return obstacle;
	}

	public static Obstacle createObstacle(float x, float y, float size, ObstacleOrientation orientation, ColorType colorType)
	{
		Obstacle obstacle = new Obstacle(colorType);
		if (orientation == ObstacleOrientation.HORIZONTAL)
		{
			obstacle.setBounds(x, y, size, OBSTACLE_SIZE);
		}
		else if (orientation == ObstacleOrientation.VERTICAL)
		{
			obstacle.setBounds(x, y, OBSTACLE_SIZE, size);
		}
		else
		{
			throw new IllegalArgumentException(String.format("Implementation for [%s] is not defined", orientation));
		}

		return obstacle;
	}

	public static List<Obstacle> createBorderObstacles()
	{
		float horizontalWidth = Gdx.graphics.getWidth() - 2 * OBSTACLE_SIZE;
		float verticalHeight = Gdx.graphics.getHeight() - 2 * OBSTACLE_SIZE;

		Obstacle leftObstacle = createObstacle(0, OBSTACLE_SIZE, verticalHeight, VERTICAL, A);
		Obstacle topObstacle = createObstacle(OBSTACLE_SIZE, verticalHeight + OBSTACLE_SIZE, horizontalWidth, HORIZONTAL, B);
		Obstacle rightObstacle = createObstacle(horizontalWidth + OBSTACLE_SIZE, OBSTACLE_SIZE, verticalHeight, VERTICAL, C);
		Obstacle bottomObstacle = createObstacle(OBSTACLE_SIZE, 0, horizontalWidth, HORIZONTAL, D);

		return Arrays.asList(leftObstacle, topObstacle, rightObstacle, bottomObstacle);
	}
}
