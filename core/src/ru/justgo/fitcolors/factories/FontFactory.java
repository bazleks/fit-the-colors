package ru.justgo.fitcolors.factories;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

import ru.justgo.fitcolors.utils.FCUtils;


public class FontFactory
{
	private static final String GUI_THREAD_NAME = "LWJGL Application";

	public static final String SMALL_FONT = "small";
	public static final String MEDIUM_FONT = "medium";
	public static final String LARGE_FONT = "large";
	public static final String HUGE_FONT = "huge";

	private static final int SMALL_FONT_SIZE = 30;
	private static final int MEDIUM_FONT_SIZE = 50;
	private static final int LARGE_FONT_SIZE = 70;
	private static final int HUGE_FONT_SIZE = 150;

	private static Map<String, BitmapFont> fonts = new HashMap<>();

	/**
	 * Only in GUI thread
	 */
	public static void initializeFonts()
	{
		System.out.println("Thread name: " + Thread.currentThread().getName());

		BitmapFont smallFont = FCUtils.createFont(Color.WHITE, SMALL_FONT_SIZE);
		BitmapFont mediumFont = FCUtils.createFont(Color.WHITE, MEDIUM_FONT_SIZE);
		BitmapFont largeFont = FCUtils.createFont(Color.WHITE, LARGE_FONT_SIZE);
		BitmapFont hugeFont = FCUtils.createFont(Color.WHITE, HUGE_FONT_SIZE);
		fonts.put(SMALL_FONT, smallFont);
		fonts.put(MEDIUM_FONT, mediumFont);
		fonts.put(LARGE_FONT, largeFont);
		fonts.put(HUGE_FONT, hugeFont);
	}

	public static BitmapFont getBitmapFont(String fontCode)
	{
		return fonts.get(fontCode);
	}

	private static void checkIfGuiThread()
	{
		if (!GUI_THREAD_NAME.equals(Thread.currentThread().getName()))
		{
			throw new IllegalThreadStateException("Font initialization must be only in GUI thread!");
		}
	}
}
