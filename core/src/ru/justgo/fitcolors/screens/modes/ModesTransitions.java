package ru.justgo.fitcolors.screens.modes;

import ru.justgo.fitcolors.enums.GameType;
import ru.justgo.fitcolors.screens.BaseScreen;

public interface ModesTransitions extends BaseScreen.ScreenTransitions {
    void toGameScreenTransition(GameType gameType);
}
