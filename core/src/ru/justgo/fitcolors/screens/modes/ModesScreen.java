package ru.justgo.fitcolors.screens.modes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.utils.Align;

import ru.justgo.fitcolors.common.ui.squaresBack.FlyingSquaresArea;
import ru.justgo.fitcolors.common.ui.squaresBack.strategies.impl.ModesScreenSquareColorStrategy;
import ru.justgo.fitcolors.enums.ColorType;
import ru.justgo.fitcolors.enums.GameType;
import ru.justgo.fitcolors.factories.FontFactory;
import ru.justgo.fitcolors.screens.BaseScreen;
import ru.justgo.fitcolors.screens.game.GameScreen;
import ru.justgo.fitcolors.screens.modes.ui.ModeTile;
import ru.justgo.fitcolors.services.Services;
import ru.justgo.fitcolors.services.ifaces.ColorSchemeService;
import ru.justgo.fitcolors.services.ifaces.GameConfigService;
import ru.justgo.fitcolors.services.ifaces.GameScoreService;
import ru.justgo.fitcolors.services.ifaces.ScreenSwitcherService;
import ru.justgo.fitcolors.services.impl.GameConfigServiceImpl;
import ru.justgo.fitcolors.utils.FCConstants;
import ru.justgo.fitcolors.utils.FCUtils;


public class ModesScreen extends BaseScreen<BaseScreen.ScreenIn, ModesTransitions>
{

	private static final int BACK_SQUARES_COUNT = 7;
	public static final String SCREEN_SWIPE_TITLE = "S C R E E N\nS W I P E";
	public static final String SQUARE_SWIPE_TITLE = "S Q U A R E\nS W I P E";
	public static final String TOUCH_SIDE_TITLE = "T O U C H\nS I D E";
	private ColorSchemeService colorSchemeService = Services.getService(ColorSchemeService.class);

	@Override
	public Color getBkColor()
	{
		return colorSchemeService.getColor(ColorType.A);
	}

	@Override
	public Actor getGUI()
	{
		Group root = new Group();

		// area of modes
		Group modesTilesArea = createModeTilesArea();

		final BitmapFont titleFont = FontFactory.getBitmapFont(FontFactory.HUGE_FONT);
		final Group labelModes = FCUtils.createShadowedLabel(titleFont, "M O D E S");
		labelModes.setPosition(0, Gdx.graphics.getHeight() * 0.75f);
		labelModes.setWidth(Gdx.graphics.getWidth());
		labelModes.setColor(FCConstants.mainScreenColor);

		FlyingSquaresArea flyingSquaresArea = new FlyingSquaresArea(BACK_SQUARES_COUNT, new ModesScreenSquareColorStrategy());
		flyingSquaresArea.setBounds(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

		root.addActor(flyingSquaresArea);
		root.addActor(labelModes);
		root.addActor(modesTilesArea);

		return root;
	}

	private Group createModeTilesArea()
	{
		Group modesArea = new Group();
		float modesAreaHeight = Gdx.graphics.getHeight() * 15f / 25f;
		modesArea.setBounds(0, 0, Gdx.graphics.getWidth(), modesAreaHeight);
		float size = Gdx.graphics.getHeight() * 9f / 25f;

		final ModeTile screenSwipeMode = new ModeTile(SCREEN_SWIPE_TITLE, GameType.SINGLE_SWIPE);
		screenSwipeMode.setBounds(2f / 45f * Gdx.graphics.getWidth(), Gdx.graphics.getHeight() * 3f / 25f, size, size);
		screenSwipeMode.setColor(FCConstants.mainScreenColor);
		screenSwipeMode.setOrigin(Align.center);
		screenSwipeMode.addListener(createTileGestureListener(screenSwipeMode, GameType.SINGLE_SWIPE));

		final ModeTile squareSwipeMode = new ModeTile(SQUARE_SWIPE_TITLE, GameType.MULTI_SWIPE);
		squareSwipeMode.setBounds(18f / 45f * Gdx.graphics.getWidth(), Gdx.graphics.getHeight() * 3f / 25f, size, size);
		squareSwipeMode.setColor(FCConstants.mainScreenColor);
		squareSwipeMode.setOrigin(Align.center);
		squareSwipeMode.addListener(createTileGestureListener(squareSwipeMode, GameType.MULTI_SWIPE));

		ModeTile touchObstacleMode = new ModeTile(TOUCH_SIDE_TITLE, GameType.TOUCH);
		touchObstacleMode.setBounds(33f / 45f * Gdx.graphics.getWidth(), Gdx.graphics.getHeight() * 3f / 25f, size, size);
		touchObstacleMode.setColor(FCConstants.mainScreenColor);
		touchObstacleMode.setOrigin(Align.center);
		touchObstacleMode.addListener(createTileGestureListener(touchObstacleMode, GameType.TOUCH));

		modesArea.addActor(screenSwipeMode);
		modesArea.addActor(squareSwipeMode);
		modesArea.addActor(touchObstacleMode);
		return modesArea;
	}

	@Override
	public ModesTransitions getScreenTransitions()
	{
		return new ModesTransitions()
		{

			private GameScoreService gameScoreService = Services.getService(GameScoreService.class);
			private ScreenSwitcherService screenSwitcherService = Services.getService(ScreenSwitcherService.class);
			private GameConfigService gameConfigService = Services.getService(GameConfigService.class);

			@Override
			public void toGameScreenTransition(final GameType gameType)
			{
				setGameType(gameType);
				gameScoreService.resetScore();
				screenSwitcherService.switchScreen(GameScreen.class);
			}

			private void setGameType(GameType gameType)
			{
				if (gameConfigService instanceof GameConfigServiceImpl)
				{
					((GameConfigServiceImpl) gameConfigService).setCurrentGameType(gameType);
				}
			}
		};

	}

	private ActorGestureListener createTileGestureListener(final ModeTile modeTile, final GameType gameType)
	{
		return new ActorGestureListener()
		{
			@Override
			public void touchDown(InputEvent event, float x, float y, int pointer, int button)
			{
				modeTile.addAction(Actions.scaleTo(1.1f, 1.1f, 0.05f));
			}

			@Override
			public void touchUp(InputEvent event, float x, float y, int pointer, int button)
			{
				modeTile.addAction(Actions.sequence(Actions.scaleTo(1f, 1f, 0.05f), Actions.run(new Runnable()
				{
					@Override
					public void run()
					{
						System.out.println(String.format("ModesScreen#gameStart(%s)", gameType));
						screenTransitions.toGameScreenTransition(gameType);
					}
				})));
			}
		};
	}
}
