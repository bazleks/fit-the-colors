package ru.justgo.fitcolors.screens.modes.ui;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Scaling;

import ru.justgo.fitcolors.common.ui.ShadowedSquare;
import ru.justgo.fitcolors.common.ui.loaders.LabelLoader;
import ru.justgo.fitcolors.common.ui.loaders.LabelLoaderTextProvider;
import ru.justgo.fitcolors.enums.GameType;
import ru.justgo.fitcolors.factories.FontFactory;
import ru.justgo.fitcolors.services.Services;
import ru.justgo.fitcolors.services.ifaces.NetworkService;
import ru.justgo.fitcolors.utils.FCConstants;
import ru.justgo.fitcolors.utils.FCUtils;


public class ModeTile extends Group
{
	private static final String CUP_PATH = "modes/cup.png";
	private static final String PEDESTAL_PATH = "modes/pedestal.jpg";

	private NetworkService networkService = Services.getService(NetworkService.class);

	private Label modeTitle;
	private LabelLoader bestScore;
	private LabelLoader bestPlace;
	private Image cupPicture;
	private Image pedestalPicture;

	private ShadowedSquare shadowedSquare;

	public ModeTile(String title, final GameType gameType)
	{
		final BitmapFont smallFont = FontFactory.getBitmapFont(FontFactory.SMALL_FONT);
		final BitmapFont mediumFont = FontFactory.getBitmapFont(FontFactory.MEDIUM_FONT);
		final BitmapFont bigFont = FontFactory.getBitmapFont(FontFactory.LARGE_FONT);

		modeTitle = FCUtils.createLabel(mediumFont, title);
		bestScore = new LabelLoader(bigFont);
		bestScore.setLabelLoaderTextProvider(new LabelLoaderTextProvider()
		{
			@Override
			public String provide()
			{
				return String.valueOf(networkService.getScoreByGameType(gameType));
			}
		});
		bestScore.update();

		bestPlace = new LabelLoader(smallFont);
		bestPlace.setLabelLoaderTextProvider(new LabelLoaderTextProvider()
		{
			@Override
			public String provide()
			{
				return String.valueOf(networkService.getBestPlaceForGameType(gameType));
			}
		});
		bestPlace.update();

		cupPicture = FCUtils.createImageFromTexture(CUP_PATH);
		pedestalPicture = FCUtils.createImageFromTexture(PEDESTAL_PATH);
	}

	@Override
	public void setBounds(float x, float y, float width, float height)
	{
		super.setBounds(x, y, width, height);

		shadowedSquare = FCUtils.createShadowedSquare(0, 0, Math.max(width, height));

		float modeTitleHeight = height * 4f / 9f;
		float bottomAreaHeight = height * 5f / 9f;

		modeTitle.setBounds(0, bottomAreaHeight, width, modeTitleHeight);
		modeTitle.setColor(FCConstants.recordUsualColor);
		Group bottomArea = createBottomArea(width, bottomAreaHeight);

		addActor(shadowedSquare);
		addActor(modeTitle);
		addActor(bottomArea);
	}

	private Group createBottomArea(float width, float height)
	{
		float cupImageSize = Math.min(width * 2f / 5f, height * 2f / 3f);
		float pedestalImageSize = Math.min(width * 2f / 5f, height * 1f / 3f);

		Group bottomArea = new Group();
		bottomArea.setBounds(0, 0, width, height);

		cupPicture.setBounds(0, 0, cupImageSize, cupImageSize);
		cupPicture.setColor(FCConstants.recordUsualColor); // todo: make cup gold/silver if score is the first or second bestPlace
		pedestalPicture.setBounds(0, cupImageSize, width * 2f / 5f, height * 1f / 3f);
		pedestalPicture.setScaling(Scaling.fit);

		bestScore.setBounds(cupImageSize, 0, width - cupImageSize, cupImageSize);
		bestScore.setColor(FCConstants.recordUsualColor);
		bestPlace.setBounds(cupImageSize, cupImageSize, width - cupImageSize, pedestalImageSize);
		bestPlace.setColor(FCConstants.recordUsualColor);

		bottomArea.addActor(cupPicture);
		bottomArea.addActor(pedestalPicture);
		bottomArea.addActor(bestScore);
		bottomArea.addActor(bestPlace);

		return bottomArea;
	}
}
