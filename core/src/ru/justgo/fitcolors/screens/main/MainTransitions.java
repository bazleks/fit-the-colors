package ru.justgo.fitcolors.screens.main;

import ru.justgo.fitcolors.screens.BaseScreen;

/**
 * @author bazleks on 8/20/2015.
 */
public interface MainTransitions extends BaseScreen.ScreenTransitions {
    void toModesScreenTransition();

    void toTopScreenTransition();

    void toMedalsScreenTransition();

    void toSettingsScreenTransition();
}
