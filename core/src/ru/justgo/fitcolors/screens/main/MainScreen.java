package ru.justgo.fitcolors.screens.main;

import static ru.justgo.fitcolors.enums.ColorType.*;
import static ru.justgo.fitcolors.utils.FCConstants.mainScreenColor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;

import ru.justgo.fitcolors.common.ui.ColorSchemeChooser;
import ru.justgo.fitcolors.common.ui.ShadowedLabel;
import ru.justgo.fitcolors.common.ui.SquareTexture;
import ru.justgo.fitcolors.common.ui.actors.squaresArea.MainSelectorSquaresArea;
import ru.justgo.fitcolors.common.ui.actors.squaresArea.SquaresArea;
import ru.justgo.fitcolors.common.ui.squaresBack.FlyingSquaresArea;
import ru.justgo.fitcolors.common.ui.squaresBack.strategies.impl.MainScreenSquareColorStrategy;
import ru.justgo.fitcolors.enums.ColorType;
import ru.justgo.fitcolors.factories.FontFactory;
import ru.justgo.fitcolors.screens.BaseScreen;
import ru.justgo.fitcolors.screens.medals.MedalsScreen;
import ru.justgo.fitcolors.screens.modes.ModesScreen;
import ru.justgo.fitcolors.screens.settings.SettingsScreen;
import ru.justgo.fitcolors.screens.top.TopScreen;
import ru.justgo.fitcolors.services.Services;
import ru.justgo.fitcolors.services.ifaces.ColorSchemeService;
import ru.justgo.fitcolors.services.ifaces.ScreenSwitcherService;
import ru.justgo.fitcolors.utils.FCUtils;


public class MainScreen extends BaseScreen<BaseScreen.ScreenIn, MainTransitions>
{

	private ColorSchemeService colorSchemeService = Services.getService(ColorSchemeService.class);

	@Override
	public Color getBkColor()
	{
		return mainScreenColor;
	}

	@Override
	public Actor getGUI()
	{
		Group root = new Group();
		SquaresArea squaresArea = createSquareArea();

		final SquareTexture shadowActor = FCUtils.createShadowTexture(squaresArea.getX(), squaresArea.getY(),
				(float) (squaresArea.getWidth() * Math.sqrt(2)));

		FlyingSquaresArea flyingSquaresArea = new FlyingSquaresArea(20, new MainScreenSquareColorStrategy(colorSchemeService));
		flyingSquaresArea.setBounds(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

		Group play = createLabel(A, "P L A Y");
		Group medals = createLabel(B, "M E D A L S");
		Group settings = createLabel(C, "S E T T I N G S");
		Group top = createLabel(D, "T O P");

		play.setPosition(squaresArea.getX(), squaresArea.getY() + squaresArea.getHeight());
		play.setWidth(squaresArea.getWidth());

		medals.setPosition(squaresArea.getX() + squaresArea.getWidth(), squaresArea.getY() + squaresArea.getHeight());
		medals.setWidth(squaresArea.getWidth());
		medals.rotateBy(-90);

		settings.setPosition(squaresArea.getX() + squaresArea.getWidth(), squaresArea.getY());
		settings.setWidth(squaresArea.getWidth());
		settings.rotateBy(180);

		top.setPosition(squaresArea.getX(), squaresArea.getY());
		top.setWidth(squaresArea.getWidth());
		top.rotateBy(90);

		root.addActor(flyingSquaresArea);
		root.addActor(shadowActor);
		root.addActor(squaresArea);

		root.addActor(play);
		root.addActor(medals);
		root.addActor(settings);
		root.addActor(top);

		ColorSchemeChooser colorChooser = new ColorSchemeChooser();
		colorChooser.initialize();
		colorChooser.setPosition(20, 20);
		colorChooser.close();

		root.addActor(colorChooser);

		return root;
	}

	@Override
	public MainTransitions getScreenTransitions()
	{
		final ScreenSwitcherService screenSwitcherService = Services.getService(ScreenSwitcherService.class);
		return new MainTransitions()
		{
			@Override
			public void toModesScreenTransition()
			{
				screenSwitcherService.switchScreen(ModesScreen.class);
			}

			@Override
			public void toTopScreenTransition()
			{
				screenSwitcherService.switchScreen(TopScreen.class);
			}

			@Override
			public void toMedalsScreenTransition()
			{
				screenSwitcherService.switchScreen(MedalsScreen.class);
			}

			@Override
			public void toSettingsScreenTransition()
			{
				screenSwitcherService.switchScreen(SettingsScreen.class);
			}
		};
	}

	private Group createLabel(ColorType type, String text)
	{
		Color color = colorSchemeService.getColor(type);
		BitmapFont font = FontFactory.getBitmapFont(FontFactory.MEDIUM_FONT);
		ShadowedLabel shadowedLabel = FCUtils.createShadowedLabel(font, text);
		shadowedLabel.setColor(color);
		return shadowedLabel;
	}

	private SquaresArea createSquareArea()
	{
		SquaresArea squaresArea = new MainSelectorSquaresArea(screenTransitions);
		setSquareAreaSize(squaresArea);
		squaresArea.initialize();

		return squaresArea;
	}

	private void setSquareAreaSize(SquaresArea squaresArea)
	{
		float size = Gdx.graphics.getHeight() * 0.75f;
		float x = (Gdx.graphics.getWidth() - size) / 2f;
		float y = Gdx.graphics.getHeight() * 0.125f;
		squaresArea.setBounds(x, y, size, size);
	}
}
