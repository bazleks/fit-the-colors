package ru.justgo.fitcolors.screens.main.listeners;

import static com.badlogic.gdx.math.Interpolation.exp5;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;

import java.util.Map;

import com.badlogic.gdx.graphics.Color;

import ru.justgo.fitcolors.enums.ObstacleType;
import ru.justgo.fitcolors.screens.game.listeners.DefaultSwipeListener;
import ru.justgo.fitcolors.common.ui.actors.Obstacle;
import ru.justgo.fitcolors.common.ui.actors.Square;


public class MainBoxSwipeListener extends DefaultSwipeListener
{
	private Map<ObstacleType, Obstacle> sidesMap;
	private Square userSquare;

	public MainBoxSwipeListener(Map<ObstacleType, Obstacle> sidesMap, Square userSquare)
	{
		this.sidesMap = sidesMap;
		this.userSquare = userSquare;
	}

	@Override
	public void swipeUp()
	{
		moveSquare(ObstacleType.MODES);
	}

	@Override
	public void swipeDown()
	{
		moveSquare(ObstacleType.SETTINGS);
	}

	@Override
	public void swipeLeft()
	{
		moveSquare(ObstacleType.TOP);
	}

	@Override
	public void swipeRight()
	{
		moveSquare(ObstacleType.MEDALS);
	}

	private void moveSquare(ObstacleType type)
	{
		float duration = 0.75f;
		float toX = 0;
		float toY = 0;

		Obstacle obstacle = sidesMap.get(type);
		Color sideColor = obstacle.getColor();

		switch (type)
		{
			case MODES:
				toX = userSquare.getX();
				toY = obstacle.getY() + obstacle.getHeight() - userSquare.getHeight();
				break;
			case MEDALS:
				toX = obstacle.getX() + obstacle.getWidth() - userSquare.getWidth();
				toY = userSquare.getY();
				break;
			case TOP:
				toX = obstacle.getX();
				toY = userSquare.getY();
				break;
			case SETTINGS:
				toX = userSquare.getX();
				toY = obstacle.getY();
				break;

		}
		if (userSquare.getActions().size != 0)
		{
			userSquare.clear();
		}
		userSquare.addAction(parallel(color(sideColor, duration, exp5), moveTo(toX, toY, duration, exp5)));
	}
}