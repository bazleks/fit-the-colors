package ru.justgo.fitcolors.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ScreenViewport;


public abstract class BaseScreen<IN extends BaseScreen.ScreenIn, TRANSITIONS extends BaseScreen.ScreenTransitions>
		extends ScreenAdapter
{

	protected IN in; // input parameters only
	protected TRANSITIONS screenTransitions;
	protected Stage stage;

	private boolean add = true;//todo:hook, must be removed

	public BaseScreen()
	{
		screenTransitions = getScreenTransitions();
	}

	@Override
	public final void show()
	{
		final Color bkColor = getBkColor();
		float r = bkColor.r;
		float g = bkColor.g;
		float b = bkColor.b;
		Gdx.gl.glClearColor(r, g, b, 1);
		stage = new Stage(new ScreenViewport());
		stage.setDebugAll(false);
		
		final Actor gui = getGUI();
		if (gui != null)
		{
			stage.addActor(gui);
			add = true;
		}
		Gdx.input.setInputProcessor(stage);
	}

	@Override
	public void render(float delta)
	{
		if (add)
		{ //TODO: this hook must be removed!!!
			stage.act(1 / 60);//TODO: this hook must be removed!!!
			add = false;//TODO: this hook must be removed!!!
		}//TODO: this hook must be removed!!!
		else
		{
			stage.act(delta);
		}
		stage.draw();
	}

	public abstract Color getBkColor();

	/**
	 * Loads user interface for a screen
	 *
	 * @return - actor with gui elements
	 */
	public abstract Actor getGUI();

	public TRANSITIONS getScreenTransitions()
	{
		return null;
	}

	public void setScreenIn(IN screenIn)
	{
		this.in = screenIn;
	}

	// interfaces

	/**
	 * empty stub - add some methods by extending this interface
	 */
	@Deprecated
	public interface ScreenIn
	{
	}

	/**
	 * please add methods on this interface by next template:
	 * prefix: to
	 * body: anything you want by 'camelWritingWay'
	 * postfix: Transition
	 * example: toHomeScreenTransition
	 */
	public interface ScreenTransitions
	{
		// empty stub - add some methods by extending this interface
	}

}