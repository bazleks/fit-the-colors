package ru.justgo.fitcolors.screens.game;

import static ru.justgo.fitcolors.enums.ColorType.*;
import static ru.justgo.fitcolors.factories.GameObjectsFactory.OBSTACLE_SIZE;
import static ru.justgo.fitcolors.screens.game.gui.SquaresCounter.Alignment.*;

import java.util.EnumMap;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;

import ru.justgo.fitcolors.enums.ColorType;
import ru.justgo.fitcolors.enums.GameType;
import ru.justgo.fitcolors.screens.BaseScreen;
import ru.justgo.fitcolors.screens.game.gui.ScoreLabel;
import ru.justgo.fitcolors.screens.game.gui.SquaresCounter;
import ru.justgo.fitcolors.screens.main.MainScreen;
import ru.justgo.fitcolors.screens.results.ResultsScreen;
import ru.justgo.fitcolors.services.Services;
import ru.justgo.fitcolors.services.ifaces.ColorSchemeService;
import ru.justgo.fitcolors.services.ifaces.GameConfigService;
import ru.justgo.fitcolors.services.ifaces.GameScoreService;
import ru.justgo.fitcolors.services.ifaces.NetworkService;
import ru.justgo.fitcolors.services.ifaces.ScreenSwitcherService;
import ru.justgo.fitcolors.utils.FCConstants;


public class GameScreen extends BaseScreen<BaseScreen.ScreenIn, GameTransitions>
{
	private Map<ColorType, SquaresCounter> counters = new EnumMap<>(ColorType.class);
	private ScoreLabel scoreLabel;

	private GameConfigService gameConfigService = Services.getService(GameConfigService.class);
	private ColorSchemeService colorSchemeService = Services.getService(ColorSchemeService.class);
	private NetworkService networkService = Services.getService(NetworkService.class);
	private GameScoreService gameScoreService = Services.getService(GameScoreService.class);

	@Override
	public Color getBkColor()
	{
		return FCConstants.mainScreenColor;
	}

	@Override
	public Actor getGUI()
	{
		Group root = new Group();
		root.setBounds(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

		scoreLabel = new ScoreLabel();
		root.addActor(scoreLabel);

		GameType selectedGameType = gameConfigService.getGameType();

		GameSquaresArea gameSquaresArea = getGameSquaresArea(selectedGameType);
		root.addActor(gameSquaresArea);

		addSquareCounters(root);

		return root;
	}

	private GameSquaresArea getGameSquaresArea(final GameType gameType)
	{
		GameEventsListener gameEventsListener = new GameEventsListener()
		{
			@Override
			public void onGameLose(GameScoreService scoreService)
			{
				networkService.sendScore(scoreService.getTotalScore(), gameType);
				screenTransitions.toStatisticsScreenTransition();
			}

			@Override
			public void onSquareFit(ColorType colorType)
			{
				gameScoreService.incrementScore(colorType);
				counters.get(colorType).refresh();
				scoreLabel.refresh();
			}
		};

		GameSquaresArea squaresArea = getSquaresArea(gameType, gameEventsListener);
		squaresArea.setBounds(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		squaresArea.initialize();

		return squaresArea;

	}

	private GameSquaresArea getSquaresArea(GameType gameType, GameEventsListener gameEventsListener)
	{
		GameSquaresArea squaresArea;
		if (gameType == GameType.SINGLE_SWIPE)
		{
			squaresArea = new SingleSquareArea(gameType, gameEventsListener);
		}
		else
		{
			squaresArea = new MultipleSquaresArea(gameType, gameEventsListener);
		}
		return squaresArea;
	}

	private SquaresCounter createSquareCounter(SquaresCounter.Alignment alignment, ColorType colorType)
	{
		SquaresCounter sqCounter = new SquaresCounter(colorType, alignment);
		sqCounter.setColor(colorSchemeService.getColor(colorType));

		int x = 0;
		int y = 0;
		int PADDING = 10;
		switch (alignment)
		{
			case LEFT_TOP:
				x = (int) OBSTACLE_SIZE + PADDING;
				y = (int) (Gdx.graphics.getHeight() - OBSTACLE_SIZE - sqCounter.getHeight() - PADDING);
				break;
			case RIGHT_TOP:
				x = (int) (Gdx.graphics.getWidth() - OBSTACLE_SIZE - sqCounter.getWidth() - PADDING);
				y = (int) (Gdx.graphics.getHeight() - OBSTACLE_SIZE - sqCounter.getHeight() - PADDING);
				break;
			case RIGHT_BOTTOM:
				x = (int) (Gdx.graphics.getWidth() - OBSTACLE_SIZE - sqCounter.getWidth() - PADDING);
				y = (int) OBSTACLE_SIZE + PADDING;
				break;
			case LEFT_BOTTOM:
				x = (int) OBSTACLE_SIZE + PADDING;
				y = (int) OBSTACLE_SIZE + PADDING;
				break;

		}

		sqCounter.setPosition(x, y);
		return sqCounter;
	}


	@Override
	public GameTransitions getScreenTransitions()
	{
		return new GameTransitions()
		{
			@Override
			public void toHomeScreenTransition()
			{
				Services.getService(ScreenSwitcherService.class).switchScreen(MainScreen.class);
			}

			@Override
			public void toStatisticsScreenTransition()
			{
				Services.getService(ScreenSwitcherService.class).switchScreen(ResultsScreen.class);
			}
		};
	}

	private void addSquareCounters(Group root)
	{
		SquaresCounter sqCounterA = createSquareCounter(LEFT_TOP, A);
		SquaresCounter sqCounterB = createSquareCounter(RIGHT_TOP, B);
		SquaresCounter sqCounterC = createSquareCounter(RIGHT_BOTTOM, C);
		SquaresCounter sqCounterD = createSquareCounter(LEFT_BOTTOM, D);

		root.addActor(sqCounterA);
		root.addActor(sqCounterB);
		root.addActor(sqCounterC);
		root.addActor(sqCounterD);

		counters.put(A, sqCounterA);
		counters.put(B, sqCounterB);
		counters.put(C, sqCounterC);
		counters.put(D, sqCounterD);
	}

}
