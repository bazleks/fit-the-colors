package ru.justgo.fitcolors.screens.game.gui;

import static ru.justgo.fitcolors.factories.FontFactory.SMALL_FONT;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.utils.Align;

import ru.justgo.fitcolors.common.ui.ShadowSquareTexture;
import ru.justgo.fitcolors.common.ui.ShadowedLabel;
import ru.justgo.fitcolors.enums.ColorType;
import ru.justgo.fitcolors.factories.FontFactory;
import ru.justgo.fitcolors.services.Services;
import ru.justgo.fitcolors.services.ifaces.ColorSchemeService;
import ru.justgo.fitcolors.services.ifaces.GameScoreService;
import ru.justgo.fitcolors.utils.FCUtils;


public class SquaresCounter extends Group
{
	private static final int SPACER_SIZE = 5;
	private static final String INITIAL_VALUE = "0";

	private ColorType colorType;
	private ShadowedLabel shadowedLabel;
	private ShadowSquareTexture squareTexture;
	private Actor spacer;

	private ColorSchemeService colorSchemeService = Services.getService(ColorSchemeService.class);
	private GameScoreService gameScoreService = Services.getService(GameScoreService.class);

	public SquaresCounter(ColorType colorType, Alignment alignment)
	{
		this.colorType = colorType;

		initActors(colorType);
		align(alignment);
	}

	private void initActors(ColorType colorType)
	{
		Color color = colorSchemeService.getColor(colorType);

		this.shadowedLabel = FCUtils.createShadowedLabel(FontFactory.getBitmapFont(SMALL_FONT), INITIAL_VALUE);
		this.shadowedLabel.setColor(color);

		this.squareTexture = new ShadowSquareTexture(color);
		this.squareTexture.setBounds(0, 0, shadowedLabel.getHeight(), shadowedLabel.getHeight());



		this.spacer = new Actor();
		this.spacer.setSize(SPACER_SIZE, SPACER_SIZE);
	}

	private void align(Alignment alignment)
	{
		if (alignment == Alignment.LEFT_TOP)
		{
			attachToLeftTop();
		}
		else if (alignment == Alignment.LEFT_BOTTOM)
		{
			attachToLeftBottom();
		}
		else if (alignment == Alignment.RIGHT_TOP)
		{
			attachToRightTop();
		}
		else if (alignment == Alignment.RIGHT_BOTTOM)
		{
			attachToRightBottom();
		}
	}

	private void attachToLeftTop()
	{
		HorizontalGroup group = new HorizontalGroup().align(Align.topLeft);
		calculateSize(group);

		group.addActor(squareTexture);
		group.addActor(spacer);
		group.addActor(shadowedLabel);

		addActor(group);
	}

	private void attachToRightBottom()
	{
		HorizontalGroup group = new HorizontalGroup().align(Align.bottomRight);
		calculateSize(group);

		group.addActor(shadowedLabel);
		group.addActor(spacer);
		group.addActor(squareTexture);

		addActor(group);
	}

	private void attachToLeftBottom()
	{
		VerticalGroup group = new VerticalGroup().align(Align.bottomLeft);
		calculateSize(group);

		group.addActor(shadowedLabel);
		group.addActor(spacer);
		group.addActor(squareTexture);

		addActor(group);
	}

	private void attachToRightTop()
	{
		VerticalGroup group = new VerticalGroup().align(Align.topRight);
		calculateSize(group);

		group.addActor(squareTexture);
		group.addActor(spacer);
		group.addActor(shadowedLabel);

		addActor(group);
	}

	private void calculateSize(Group group)
	{
		int width = 0;
		int height = 0;

		if (group instanceof HorizontalGroup)
		{
			width = (int) (squareTexture.getWidth() + shadowedLabel.getWidth() + spacer.getWidth());
			height = (int) Math.max(squareTexture.getHeight(), shadowedLabel.getHeight());
		}
		else if (group instanceof VerticalGroup)
		{
			width = (int) Math.max(squareTexture.getWidth(), shadowedLabel.getWidth());
			height = (int) (squareTexture.getHeight() + shadowedLabel.getHeight() + spacer.getHeight());
		}
		group.setSize(width, height);
		setSize(width, height);
	}

	@Override
	public void setBounds(float x, float y, float width, float height)
	{
		super.setBounds(x, y, width, height);
		refresh();
	}

	public void refresh()
	{
		int count = gameScoreService.getScoreByColor(colorType);
		shadowedLabel.setText(Integer.toString(count));
	}

	public enum Alignment
	{

		LEFT_BOTTOM, RIGHT_BOTTOM, LEFT_TOP, RIGHT_TOP
	}
}
