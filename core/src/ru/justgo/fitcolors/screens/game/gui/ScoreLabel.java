package ru.justgo.fitcolors.screens.game.gui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Label;

import ru.justgo.fitcolors.factories.FontFactory;
import ru.justgo.fitcolors.services.Services;
import ru.justgo.fitcolors.services.ifaces.GameScoreService;
import ru.justgo.fitcolors.utils.FCUtils;


public class ScoreLabel extends Group
{

	private Label label;
	private GameScoreService gameScoreService = Services.getService(GameScoreService.class);

	public ScoreLabel()
	{
		BitmapFont shadowedFont = FontFactory.getBitmapFont(FontFactory.HUGE_FONT);

		label = FCUtils.createLabel(shadowedFont, "0");
		label.setBounds(0, Gdx.graphics.getHeight() * 3f / 4f, Gdx.graphics.getWidth(), Gdx.graphics.getHeight() / 4f);
		label.setColor(new Color(0, 0, 0, 0.1f));
		addActor(label);
	}

	public void refresh()
	{
		int score = gameScoreService.getTotalScore();
		label.setText(Integer.toString(score));
	}

	public void setGameScoreService(GameScoreService gameScoreService)
	{
		this.gameScoreService = gameScoreService;
	}
}
