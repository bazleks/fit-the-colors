package ru.justgo.fitcolors.screens.game;

import ru.justgo.fitcolors.services.ifaces.GameScoreService;
import ru.justgo.fitcolors.enums.ColorType;

public interface GameEventsListener {

    void onGameLose(GameScoreService scoreService);

    void onSquareFit(ColorType colorType);
}
