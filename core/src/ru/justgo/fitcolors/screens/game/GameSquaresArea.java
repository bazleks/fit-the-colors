package ru.justgo.fitcolors.screens.game;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.alpha;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

import java.util.List;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

import ru.justgo.fitcolors.common.ui.actors.Obstacle;
import ru.justgo.fitcolors.common.ui.actors.Square;
import ru.justgo.fitcolors.common.ui.actors.squaresArea.SquaresArea;
import ru.justgo.fitcolors.enums.GameType;
import ru.justgo.fitcolors.factories.GameObjectsFactory;
import ru.justgo.fitcolors.screens.game.controllers.SquareController;
import ru.justgo.fitcolors.screens.game.controllers.squaresarea.UserController;
import ru.justgo.fitcolors.screens.game.controllers.squaresarea.impl.SquaresAreaSwipeController;
import ru.justgo.fitcolors.screens.game.controllers.squaresarea.impl.SquaresAreaTouchController;
import ru.justgo.fitcolors.screens.game.listeners.MultiSwipeListener;
import ru.justgo.fitcolors.services.Services;
import ru.justgo.fitcolors.services.ifaces.GameScoreService;
import ru.justgo.fitcolors.utils.collisions.CollisionListener;


public class GameSquaresArea extends SquaresArea
{
	private GameEventsListener gameEventsListener;
	private GameType gameType;

	private GameScoreService gameScoreService = Services.getService(GameScoreService.class);

	public GameSquaresArea(GameType gameType, GameEventsListener gameEventsListener)
	{
		this.gameType = gameType;
		this.gameEventsListener = gameEventsListener;
	}

	@Override
	public void initialize()
	{
		super.initialize();
		addBorderObstacles();
	}

	private void addBorderObstacles()
	{
		List<Obstacle> borderObstacles = GameObjectsFactory.createBorderObstacles();
		for (Obstacle borderObstacle : borderObstacles)
		{
			addActor(borderObstacle);
		}
	}

	@Override
	protected CollisionListener createCollisionListener()
	{
		return new CollisionListener()
		{
			@Override
			public void onSquareHitObstacle(Square square, Obstacle obstacle)
			{
				boolean isSolid = square.getColor().a == 1f && obstacle.getColor().a == 1f;
				if (isSolid)
				{
					boolean isSameColors = obstacle.getColorType() == square.getColorType();
					if (isSameColors)
					{
						//@formatter:off
                        square.addAction(
                        		sequence(
                        				alpha(0f, 0.1f),
												newPlaceAction(square),
												alpha(1f, 1.5f)));
                        //@formatter:on
						gameEventsListener.onSquareFit(square.getColorType());
					}
					else
					{
						gameEventsListener.onGameLose(gameScoreService);
					}
				}
			}

			private Action newPlaceAction(final Square square)
			{
				return Actions.run(new Runnable()
				{
					@Override
					public void run()
					{
						new SquareController(square).setRandomStartingParameters(GameSquaresArea.this);
					}
				});
			}
		};
	}

	@Override
	protected UserController createUserController()
	{
		UserController controller;

		switch (gameType)
		{
			case MULTI_SWIPE:
				controller = createMultiSwipeController();
				break;
			case TOUCH:
				controller = createTouchObstacleController();
				break;
			default:
				throw new IllegalArgumentException(String.format("Controller for [%s] game mode is not implemented yet", gameType));
		}

		return controller;
	}

	private SquaresAreaTouchController createTouchObstacleController()
	{
		return new SquaresAreaTouchController(this);
	}

	private SquaresAreaSwipeController createMultiSwipeController()
	{
		SquaresAreaSwipeController controller = new SquaresAreaSwipeController();
		controller.setSquaresArea(this);
		controller.setSwipeListener(new MultiSwipeListener());
		return controller;
	}
}
