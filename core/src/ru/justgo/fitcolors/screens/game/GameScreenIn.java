package ru.justgo.fitcolors.screens.game;

import ru.justgo.fitcolors.enums.GameType;
import ru.justgo.fitcolors.screens.BaseScreen;
import ru.justgo.fitcolors.services.ifaces.ColorSchemeService;
import ru.justgo.fitcolors.services.ifaces.ObjectsSizeCalculatorService;
import ru.justgo.fitcolors.services.ifaces.SquareSpeedService;
import ru.justgo.fitcolors.services.ifaces.GameScoreService;

/**
 * @author bazleks
 */
public interface GameScreenIn extends BaseScreen.ScreenIn {
    GameType getGameType();

    GameScoreService getGameScoreService();

    SquareSpeedService getSquareSpeedService();

    ObjectsSizeCalculatorService getObjectsSizeCalculatorService();

    ColorSchemeService getColorSchemeService();
}
