package ru.justgo.fitcolors.screens.game;

import ru.justgo.fitcolors.screens.BaseScreen;

/**
 * @author bazleks on 8/19/2015.
 */
public interface GameTransitions extends BaseScreen.ScreenTransitions {
    void toHomeScreenTransition();

    void toStatisticsScreenTransition();
}
