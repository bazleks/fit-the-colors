package ru.justgo.fitcolors.screens.game;

import com.badlogic.gdx.math.MathUtils;

import ru.justgo.fitcolors.common.ui.actors.Obstacle;
import ru.justgo.fitcolors.common.ui.actors.Square;
import ru.justgo.fitcolors.enums.GameType;
import ru.justgo.fitcolors.factories.GameObjectsFactory;
import ru.justgo.fitcolors.screens.game.controllers.squaresarea.UserController;
import ru.justgo.fitcolors.screens.game.controllers.squaresarea.impl.SquaresAreaSwipeController;
import ru.justgo.fitcolors.screens.game.listeners.SingleSwipeListener;
import ru.justgo.fitcolors.screens.game.strategies.ActorAdditionStrategy;


public class SingleSquareArea extends GameSquaresArea
{
	private Square activeSquare;
	private ActorAdditionStrategy barrierAdditionStrategy = new ActorAdditionStrategy()
	{
		@Override
		public void addActor()
		{
			Obstacle obstacle = ru.justgo.fitcolors.factories.GameObjectsFactory.createObstacle(MathUtils.random(5f, 10f), SingleSquareArea.this);
			SingleSquareArea.this.addActor(obstacle);
		}
	};

	public SingleSquareArea(GameType gameType, GameEventsListener gameEventsListener)
	{
		super(gameType, gameEventsListener);
	}

	@Override
	public void initialize()
	{
		activeSquare = GameObjectsFactory.createSquare(this);
		addActor(activeSquare);

		super.initialize();
	}

	@Override
	public void act(float delta)
	{
		super.act(delta);
		barrierAdditionStrategy.act(delta);
	}

	@Override
	protected UserController createUserController()
	{
		System.out.println(String.format("SingleSquareArea#createUserController (activeSquare = [%s])", activeSquare));
		return createSingleSwipeController();
	}

	private SquaresAreaSwipeController createSingleSwipeController()
	{
		SingleSwipeListener singleSwipeListener = new SingleSwipeListener(activeSquare);

		SquaresAreaSwipeController controller = new SquaresAreaSwipeController();
		controller.setSwipeListener(singleSwipeListener);
		controller.setSquaresArea(this);
		return controller;
	}
}
