package ru.justgo.fitcolors.screens.game.strategies;

import ru.justgo.fitcolors.services.Services;
import ru.justgo.fitcolors.services.ifaces.GameScoreService;


public abstract class ActorAdditionStrategy
{
	private static final float MAX_TIME_PERIOD = 10f;
	private static final float MIN_TIME_PERIOD = 3f;
	private static final float SCORE_WITH_MIN_TIME_PERIOD = 100f;

	private float timeAfterLastSquareAdd = 0;
	private float newSquareTimePeriod = 0;

	private GameScoreService gameScoreService = Services.getService(GameScoreService.class);

	public void act(float deltaTime)
	{
		if (timeAfterLastSquareAdd >= newSquareTimePeriod)
		{
			addActor();
			timeAfterLastSquareAdd = 0;
			calculateNewSquareTimePeriod();
		}
		timeAfterLastSquareAdd += deltaTime;
	}

	private void calculateNewSquareTimePeriod()
	{
		int totalScore = gameScoreService.getTotalScore();
		if (totalScore == SCORE_WITH_MIN_TIME_PERIOD)
		{
			newSquareTimePeriod = MIN_TIME_PERIOD;
		}
		else
		{
			float coefficient = (float) totalScore / SCORE_WITH_MIN_TIME_PERIOD;
			newSquareTimePeriod = MAX_TIME_PERIOD - (MAX_TIME_PERIOD - MIN_TIME_PERIOD) * coefficient;
		}
	}

	public abstract void addActor();
}
