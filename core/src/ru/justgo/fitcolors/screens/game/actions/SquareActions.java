package ru.justgo.fitcolors.screens.game.actions;

import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.RotateByAction;


public class SquareActions
{
	private static final float ROTATION_SPEED = 0.25f;
	private static final float QUADRANT_ROTATION = 90f;

	private SquareActions()
	{
	}

	public static RotateByAction clockwiseRotation()
	{
		return Actions.rotateBy(-QUADRANT_ROTATION, ROTATION_SPEED);
	}

	public static RotateByAction counterClockwiseRotation()
	{
		return Actions.rotateBy(QUADRANT_ROTATION, ROTATION_SPEED);
	}
}
