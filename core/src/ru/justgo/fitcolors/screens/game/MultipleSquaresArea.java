package ru.justgo.fitcolors.screens.game;

import ru.justgo.fitcolors.common.ui.actors.Square;
import ru.justgo.fitcolors.enums.GameType;
import ru.justgo.fitcolors.factories.GameObjectsFactory;
import ru.justgo.fitcolors.screens.game.strategies.ActorAdditionStrategy;


public class MultipleSquaresArea extends GameSquaresArea
{
	private ActorAdditionStrategy actorAdditionStrategy = new ActorAdditionStrategy()
	{
		@Override
		public void addActor()
		{
			Square square = GameObjectsFactory.createSquare(MultipleSquaresArea.this);
			MultipleSquaresArea.this.addActor(square);
		}
	};

	public MultipleSquaresArea(GameType gameType, GameEventsListener gameEventsListener)
	{
		super(gameType, gameEventsListener);
	}

	@Override
	public void initialize()
	{
		super.initialize();
	}

	@Override
	public void act(float delta)
	{
		super.act(delta);
		actorAdditionStrategy.act(delta);
	}


}
