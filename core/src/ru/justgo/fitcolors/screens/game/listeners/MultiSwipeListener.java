package ru.justgo.fitcolors.screens.game.listeners;


import ru.justgo.fitcolors.common.ui.actors.Square;
import ru.justgo.fitcolors.screens.game.controllers.SquareController;


public class MultiSwipeListener extends DefaultSwipeListener
{

	private SquareController squareController;

	@Override
	public void swipeUp()
	{
		if (touchDownActor instanceof Square)
		{
			moveUp((Square) touchDownActor);
		}
	}

	@Override
	public void swipeDown()
	{
		if (touchDownActor instanceof Square)
		{
			moveDown((Square) touchDownActor);
		}
	}

	@Override
	public void swipeLeft()
	{
		if (touchDownActor instanceof Square)
		{
			moveLeft((Square) touchDownActor);
		}
	}

	@Override
	public void swipeRight()
	{
		if (touchDownActor instanceof Square)
		{
			moveRight((Square) touchDownActor);
		}
	}

	private void moveUp(Square square)
	{
		squareController = new SquareController(square);
		squareController.swipeUp();
		System.out.println(String.format("MOVE [%s] UP", square));
	}

	private void moveDown(Square square)
	{
		squareController = new SquareController(square);
		squareController.swipeDown();
		System.out.println(String.format("MOVE [%s] DOWN", square));
	}

	private void moveLeft(Square square)
	{
		squareController = new SquareController(square);
		squareController.swipeLeft();
		System.out.println(String.format("MOVE [%s] LEFT", square));
	}

	private void moveRight(Square square)
	{
		squareController = new SquareController(square);
		squareController.swipeRight();
		System.out.println(String.format("MOVE [%s] RIGHT", square));
	}
}
