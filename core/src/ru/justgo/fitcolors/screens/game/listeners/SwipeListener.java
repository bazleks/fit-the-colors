package ru.justgo.fitcolors.screens.game.listeners;

import com.badlogic.gdx.scenes.scene2d.Actor;


public interface SwipeListener
{
	void swipeUp();

	void swipeDown();

	void swipeLeft();

	void swipeRight();

	void setTouchedActor(Actor touchDownActor);
}
