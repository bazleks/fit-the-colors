package ru.justgo.fitcolors.screens.game.listeners;

import com.badlogic.gdx.scenes.scene2d.Actor;


public abstract class DefaultSwipeListener implements SwipeListener
{
	protected Actor touchDownActor;

	@Override
	public void swipeUp()
	{
	}

	@Override
	public void swipeDown()
	{
	}

	@Override
	public void swipeLeft()
	{
	}

	@Override
	public void swipeRight()
	{
	}

	@Override
	public void setTouchedActor(Actor touchDownActor)
	{
		this.touchDownActor = touchDownActor;
	}
}
