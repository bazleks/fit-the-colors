package ru.justgo.fitcolors.screens.game.listeners;

import ru.justgo.fitcolors.common.ui.actors.Square;
import ru.justgo.fitcolors.screens.game.controllers.SquareController;


public class SingleSwipeListener extends DefaultSwipeListener {
    private SquareController squareController;

    public SingleSwipeListener(Square activeSquare) {
        squareController = new SquareController(activeSquare);
    }

    @Override
    public void swipeUp() {
        squareController.swipeUp();
    }

    @Override
    public void swipeDown() {
        squareController.swipeDown();
    }

    @Override
    public void swipeLeft() {
        squareController.swipeLeft();
    }

    @Override
    public void swipeRight() {
        squareController.swipeRight();
    }
}
