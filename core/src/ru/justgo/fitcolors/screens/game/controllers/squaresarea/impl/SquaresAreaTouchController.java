package ru.justgo.fitcolors.screens.game.controllers.squaresarea.impl;

import static ru.justgo.fitcolors.enums.ColorChooserDirection.*;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;

import ru.justgo.fitcolors.common.ui.ColorChooser;
import ru.justgo.fitcolors.common.ui.ColorTypedSquare;
import ru.justgo.fitcolors.common.ui.actors.Obstacle;
import ru.justgo.fitcolors.common.ui.actors.squaresArea.SquaresArea;
import ru.justgo.fitcolors.enums.ColorChooserDirection;
import ru.justgo.fitcolors.screens.game.controllers.squaresarea.UserController;


public class SquaresAreaTouchController implements UserController
{

	private ColorChooser colorChooser;

	public SquaresAreaTouchController(SquaresArea gameSquaresArea)
	{
		this.colorChooser = new ColorChooser();

		gameSquaresArea.addActor(colorChooser);
		colorChooser.getColor().a = 0f;
		colorChooser.close();

		colorChooser.addListener(new ActorGestureListener()
		{

			@Override
			public void touchUp(InputEvent event, float x, float y, int pointer, int button)
			{
				Actor hit = colorChooser.hit(x, y, true);
				if (hit instanceof ColorTypedSquare)
				{
					colorChooser.setSelectedColorType(((ColorTypedSquare) hit).getColorType());
				}

				colorChooser.changeObstacleColor();
			}
		});

	}

	@Override
	public ActorGestureListener getActorGestureListener()
	{
		return new ActorGestureListener()
		{
			boolean flag = false;

			@Override
			public void touchDown(InputEvent event, float x, float y, int pointer, int button)
			{
				colorChooser.setPosition(x, y);
				Actor touchDownTarget = getTouchDownTarget();
				if (touchDownTarget instanceof Obstacle)
				{
					colorChooser.setVisible(true);
					ColorChooserDirection direction = defineColorChooserDirection((Obstacle) touchDownTarget);
					colorChooser.open(direction, (int) x, (int) y);
					colorChooser.setSelectedObstacle((Obstacle) touchDownTarget);
					flag = true;
				}
			}

			private ColorChooserDirection defineColorChooserDirection(Obstacle obstacle)
			{
				if (obstacle.getWidth() > obstacle.getHeight())
				{
					if (obstacle.getY() < Gdx.graphics.getHeight() / 2)
					{
						return TO_UP;
					}
					else
					{
						return TO_DOWN;
					}
				}
				else
				{
					if (obstacle.getX() < Gdx.graphics.getWidth() / 2)
					{
						return TO_RIGHT;
					}
					else
					{
						return TO_LEFT;
					}
				}
			}

			@Override
			public void touchUp(InputEvent event, float x, float y, int pointer, int button)
			{
				if (flag)
				{
					flag = false;
					colorChooser.close();
					colorChooser.setVisible(false);
					colorChooser.fire(event);

				}

			}
		};
	}
}
