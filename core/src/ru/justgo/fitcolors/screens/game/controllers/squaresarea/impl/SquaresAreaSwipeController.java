package ru.justgo.fitcolors.screens.game.controllers.squaresarea.impl;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;

import ru.justgo.fitcolors.screens.game.listeners.SwipeListener;
import ru.justgo.fitcolors.screens.game.controllers.squaresarea.UserController;
import ru.justgo.fitcolors.common.ui.actors.squaresArea.SquaresArea;


public class SquaresAreaSwipeController implements UserController
{
	enum SwipeType
	{
		LEFT, RIGHT, UP, DOWN
	}

	private SquaresArea squaresArea;
	private SwipeListener swipeListener;

	public void setSquaresArea(SquaresArea squaresArea)
	{
		this.squaresArea = squaresArea;
	}

	@Override
	public ActorGestureListener getActorGestureListener()
	{
		return new ActorGestureListener()
		{
			SwipeType currentSwipe;

			@Override
			public void touchDown(InputEvent event, float x, float y, int pointer, int button)
			{
				if (squaresArea != null)
				{
					Actor touchDownActor = squaresArea.hit(x, y, true);
					swipeListener.setTouchedActor(touchDownActor);
				}
			}
			@Override
			public void fling(InputEvent event, float velocityX, float velocityY, int button)
			{
				if (Math.abs(velocityX) > Math.abs(velocityY))
				{
					if (velocityX > 0)
					{
						currentSwipe = SwipeType.RIGHT;
					}
					else
					{
						currentSwipe = SwipeType.LEFT;
					}
				}
				else
				{
					if (velocityY > 0)
					{
						currentSwipe = SwipeType.UP;
					}
					else
					{
						currentSwipe = SwipeType.DOWN;
					}
				}
			}

			@Override
			public void touchUp(InputEvent event, float x, float y, int pointer, int button)
			{
				if (currentSwipe == null)
					return;
				switch (currentSwipe)
				{
					case LEFT:
						swipeListener.swipeLeft();
						break;
					case RIGHT:
						swipeListener.swipeRight();
						break;
					case UP:
						swipeListener.swipeUp();
						break;
					case DOWN:
						swipeListener.swipeDown();
						break;
				}
				currentSwipe = null;
			}
		};
	}

	public void setSwipeListener(SwipeListener swipeListener)
	{
		this.swipeListener = swipeListener;
	}
}
