package ru.justgo.fitcolors.screens.game.controllers.squaresarea;

import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;


public interface UserController
{
	ActorGestureListener getActorGestureListener();
}
