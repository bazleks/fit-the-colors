package ru.justgo.fitcolors.screens.game.controllers;

import static ru.justgo.fitcolors.enums.VelocityVector.*;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

import ru.justgo.fitcolors.common.ui.actors.Square;
import ru.justgo.fitcolors.enums.ColorType;
import ru.justgo.fitcolors.enums.VelocityVector;
import ru.justgo.fitcolors.screens.game.GameSquaresArea;
import ru.justgo.fitcolors.screens.game.actions.SquareActions;
import ru.justgo.fitcolors.services.Services;
import ru.justgo.fitcolors.services.ifaces.GameScoreService;
import ru.justgo.fitcolors.services.ifaces.ObjectsSizeCalculatorService;
import ru.justgo.fitcolors.services.ifaces.SquareSpeedService;
import ru.justgo.fitcolors.utils.FCUtils;


public class SquareController
{
	private Square square;

	private ObjectsSizeCalculatorService sizeCalculatorService = Services.getService(ObjectsSizeCalculatorService.class);

	public SquareController(Square square)
	{
		this.square = square;
	}

	public void swipeUp()
	{
		Vector2 velocity = square.getVelocity();
		VelocityVector squareDirection = defineDirection(velocity);
		boolean horizontal = squareDirection == LEFT || squareDirection == RIGHT;
		if (horizontal)
		{
			square.setVelocity(new Vector2(0, Math.abs(velocity.x)));
			if (squareDirection == LEFT)
			{
				square.addAction(SquareActions.counterClockwiseRotation());
			}
			if (squareDirection == RIGHT)
			{
				square.addAction(SquareActions.clockwiseRotation());
			}
		}
		if (squareDirection == UP)
		{
			square.accelerateSquare();
		}
	}

	public void swipeDown()
	{
		Vector2 velocity = square.getVelocity();
		VelocityVector direction = defineDirection(velocity);
		boolean horizontal = direction == LEFT || direction == RIGHT;
		if (horizontal)
		{
			square.setVelocity(new Vector2(0, -Math.abs(velocity.x)));
			if (direction == LEFT)
			{
				square.addAction(SquareActions.clockwiseRotation());
			}
			if (direction == RIGHT)
			{
				square.addAction(SquareActions.counterClockwiseRotation());
			}
		}
	}

	public void swipeLeft()
	{
		Vector2 velocity = square.getVelocity();
		VelocityVector direction = defineDirection(velocity);
		boolean vertical = direction == UP || direction == DOWN;
		if (vertical)
		{
			square.setVelocity(new Vector2(-Math.abs(velocity.y), 0));
			if (direction == UP)
			{
				square.addAction(SquareActions.clockwiseRotation());
			}
			if (direction == DOWN)
			{
				square.addAction(SquareActions.counterClockwiseRotation());
			}
		}
	}

	public void swipeRight()
	{
		Vector2 velocity = square.getVelocity();
		VelocityVector direction = defineDirection(velocity);
		boolean vertical = direction == UP || direction == DOWN;
		if (vertical)
		{
			square.setVelocity(new Vector2(Math.abs(velocity.y), 0));
			if (direction == UP)
			{
				square.addAction(SquareActions.counterClockwiseRotation());
			}
			if (direction == DOWN)
			{
				square.addAction(SquareActions.clockwiseRotation());
			}
		}
	}

	public void setRandomStartingParameters(GameSquaresArea gameSquaresArea)
	{
		VelocityVector randomDirection = FCUtils.getRandFromEnum(VelocityVector.class);
		ColorType randomColorType = FCUtils.getRandFromEnum(ColorType.class);
		Vector2 startingPoint = defineStartingPoint(randomDirection, gameSquaresArea);
		Vector2 squareSpeed = getSquareSpeed(randomDirection);

		square.setPosition(startingPoint.x, startingPoint.y);
		square.setColorType(randomColorType);
		square.addAction(Actions.sequence(Actions.alpha(0f), Actions.alpha(1f, 1f)));
		square.setVelocity(squareSpeed);
	}

	private Vector2 getSquareSpeed(VelocityVector velocityVector)
	{
		int score = Services.getService(GameScoreService.class).getTotalScore();
		float speedModule = Services.getService(SquareSpeedService.class).squareSpeed(score);

		return velocityVector.getVelocity().cpy().scl(speedModule);
	}

	private Vector2 defineStartingPoint(VelocityVector randomDirection, GameSquaresArea gameSquaresArea)
	{
		float obstacleSize = sizeCalculatorService.getObstacleSize();
		float x;
		float y;
		switch (randomDirection)
		{
			case LEFT:
				x = Gdx.graphics.getWidth();
				y = MathUtils.random(obstacleSize, gameSquaresArea.getHeight() - obstacleSize - square.getHeight());
				break;
			case RIGHT:
				x = -square.getWidth();
				y = MathUtils.random(obstacleSize, gameSquaresArea.getHeight() - obstacleSize - square.getHeight());
				break;
			case UP:
				x = MathUtils.random(obstacleSize, gameSquaresArea.getWidth() - obstacleSize - square.getWidth());
				y = -square.getHeight();
				break;
			case DOWN:
				x = MathUtils.random(obstacleSize, gameSquaresArea.getWidth() - obstacleSize - square.getWidth());
				y = Gdx.graphics.getHeight();
				break;
			default:
				throw new IllegalArgumentException(
						String.format("Cannot define starting point for direction = [%s]", randomDirection));
		}
		return new Vector2(x, y);
	}

	private VelocityVector defineDirection(Vector2 velocity)
	{
		if (velocity.x > 0)
		{
			return RIGHT;
		}
		if (velocity.x < 0)
		{
			return LEFT;
		}
		if (velocity.y > 0)
		{
			return UP;
		}
		if (velocity.y < 0)
		{
			return DOWN;
		}

		throw new IllegalStateException("Square is stopped!");
	}
}
