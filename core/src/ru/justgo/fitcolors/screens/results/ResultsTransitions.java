package ru.justgo.fitcolors.screens.results;

import ru.justgo.fitcolors.screens.BaseScreen;

/**
 * @author bazleks on 8/26/2015.
 */
public interface ResultsTransitions extends BaseScreen.ScreenTransitions {
    void toMainScreenTransition();

    void toGameScreenTransition();
}
