package ru.justgo.fitcolors.screens.results;

import ru.justgo.fitcolors.screens.BaseScreen;
import ru.justgo.fitcolors.services.ifaces.ColorSchemeService;
import ru.justgo.fitcolors.services.ifaces.GameScoreService;

/**
 * @author bazleks on 1/31/2016.
 */
public interface ResultsIn extends BaseScreen.ScreenIn {
    ColorSchemeService getColorSchemeService();

    GameScoreService getScoreService();

    
}
