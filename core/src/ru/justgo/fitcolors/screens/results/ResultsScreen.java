package ru.justgo.fitcolors.screens.results;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.utils.Align;

import ru.justgo.fitcolors.common.ui.loaders.LabelLoader;
import ru.justgo.fitcolors.common.ui.loaders.LabelLoaderTextProvider;
import ru.justgo.fitcolors.enums.ColorType;
import ru.justgo.fitcolors.enums.GameType;
import ru.justgo.fitcolors.factories.FontFactory;
import ru.justgo.fitcolors.screens.BaseScreen;
import ru.justgo.fitcolors.screens.game.GameScreenIn;
import ru.justgo.fitcolors.screens.game.gui.SquaresCounter;
import ru.justgo.fitcolors.screens.main.MainScreen;
import ru.justgo.fitcolors.services.Services;
import ru.justgo.fitcolors.services.ifaces.ColorSchemeService;
import ru.justgo.fitcolors.services.ifaces.GameScoreService;
import ru.justgo.fitcolors.services.ifaces.NetworkService;
import ru.justgo.fitcolors.services.ifaces.ObjectsSizeCalculatorService;
import ru.justgo.fitcolors.services.ifaces.ScreenSwitcherService;
import ru.justgo.fitcolors.services.ifaces.SquareSpeedService;
import ru.justgo.fitcolors.services.impl.SquareSpeedServiceImpl;
import ru.justgo.fitcolors.utils.FCConstants;
import ru.justgo.fitcolors.utils.FCUtils;


public class ResultsScreen extends BaseScreen<ResultsIn, ResultsTransitions>
{

	public static final int SPACE = 75;
	public static final int BUTTON_SIZE = 75;

	private GameScoreService gameScoreService = Services.getService(GameScoreService.class);
	private ColorSchemeService colorSchemeService = Services.getService(ColorSchemeService.class);
	private NetworkService networkService = Services.getService(NetworkService.class);

	@Override
	public Color getBkColor()
	{
		return FCConstants.mainScreenColor;
	}

	@Override
	public Actor getGUI()
	{
		final int totalScore = gameScoreService.getTotalScore();

		VerticalGroup group = new VerticalGroup();
		group.space(10);

		BitmapFont shadowedSmallFont = FontFactory.getBitmapFont(FontFactory.SMALL_FONT);
		BitmapFont shadowedLargeFont = FontFactory.getBitmapFont(FontFactory.LARGE_FONT);

		LabelLoader placeLabel = new LabelLoader(shadowedSmallFont);
		placeLabel.setLabelLoaderTextProvider(new LabelLoaderTextProvider()
		{
			@Override
			public String provide()
			{
				return String.valueOf(networkService.getPlaceForScore(GameType.SINGLE_SWIPE, totalScore));
			}
		});
		placeLabel.setColor(Color.BLACK);
		placeLabel.update();
		Image placeCup = FCUtils.createImageFromTexture("modes/cup.png", 30, 30);
		placeCup.setColor(FCConstants.recordUsualColor);

		LabelLoader scoreLabel = new LabelLoader(shadowedLargeFont);
		scoreLabel.setLabelLoaderTextProvider(new LabelLoaderTextProvider()
		{
			@Override
			public String provide()
			{
				return String.valueOf(gameScoreService.getTotalScore());
			}
		});
		scoreLabel.setColor(FCConstants.recordUsualColor);
		scoreLabel.update();

		SquaresCounter squaresA = new SquaresCounter(ColorType.A, SquaresCounter.Alignment.LEFT_TOP);
		SquaresCounter squaresB = new SquaresCounter(ColorType.B, SquaresCounter.Alignment.LEFT_TOP);
		SquaresCounter squaresC = new SquaresCounter(ColorType.C, SquaresCounter.Alignment.LEFT_TOP);
		SquaresCounter squaresD = new SquaresCounter(ColorType.D, SquaresCounter.Alignment.LEFT_TOP);

		Image restartButton = FCUtils.createImageFromTexture("results/restart.png", BUTTON_SIZE, BUTTON_SIZE);
		restartButton.addListener(new ActorGestureListener()
		{
			@Override
			public void tap(InputEvent event, float x, float y, int count, int button)
			{
				Services.getService(GameScoreService.class).resetScore();
				getScreenTransitions().toGameScreenTransition();
			}
		});

		Image homeButton = FCUtils.createImageFromTexture("results/home.png", BUTTON_SIZE, BUTTON_SIZE);
		homeButton.addListener(new ActorGestureListener()
		{
			@Override
			public void tap(InputEvent event, float x, float y, int count, int button)
			{
				System.out.println("ResultsScreen.tap");
				getScreenTransitions().toMainScreenTransition();
			}
		});

		HorizontalGroup placeRow = new HorizontalGroup();
		placeRow.addActor(placeCup);
		placeRow.addActor(placeLabel);

		HorizontalGroup scoreRow = new HorizontalGroup();
		scoreRow.addActor(scoreLabel);

		HorizontalGroup squareCountersRow = new HorizontalGroup();
		squareCountersRow.space(30);
		squareCountersRow.addActor(squaresA);
		squareCountersRow.addActor(squaresB);
		squareCountersRow.addActor(squaresC);
		squareCountersRow.addActor(squaresD);

		HorizontalGroup buttonsRow = new HorizontalGroup();
		buttonsRow.padTop(20);
		buttonsRow.space(20);
		buttonsRow.addActor(restartButton);
		buttonsRow.addActor(homeButton);

		group.addActor(placeRow);
		group.addActor(scoreRow);
		group.addActor(squareCountersRow);
		group.addActor(buttonsRow);

		group.pack();

		System.out.println("Place row width = " + placeRow.getWidth());


		Container<VerticalGroup> root = new Container<>();
		root.setFillParent(true);
		root.setOrigin(Align.center);
		root.setActor(group);
		return root;
	}


	@Override
	public ResultsTransitions getScreenTransitions()
	{
		return new ResultsTransitions()
		{
			@Override
			public void toMainScreenTransition()
			{
				Services.getService(ScreenSwitcherService.class).switchScreen(MainScreen.class);
			}

			@Override
			public void toGameScreenTransition()
			{
				Services.getService(ScreenSwitcherService.class).switchScreenBack(new GameScreenIn()
				{
					@Override
					public GameType getGameType()
					{
						return GameType.SINGLE_SWIPE;
					}

					@Override
					public GameScoreService getGameScoreService()
					{
						return Services.getService(GameScoreService.class);
					}

					@Override
					public SquareSpeedService getSquareSpeedService()
					{
						return new SquareSpeedServiceImpl();
					}

					@Override
					public ObjectsSizeCalculatorService getObjectsSizeCalculatorService()
					{
						return Services.getService(ObjectsSizeCalculatorService.class);
					}

					@Override
					public ColorSchemeService getColorSchemeService()
					{
						return Services.getService(ColorSchemeService.class);
					}
				});
			}
		};
	}
}
