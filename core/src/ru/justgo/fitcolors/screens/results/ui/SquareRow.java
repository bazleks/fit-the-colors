package ru.justgo.fitcolors.screens.results.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;

import ru.justgo.fitcolors.common.ui.SquareTexture;
import ru.justgo.fitcolors.enums.ColorType;
import ru.justgo.fitcolors.factories.FontFactory;
import ru.justgo.fitcolors.services.ifaces.ColorSchemeService;
import ru.justgo.fitcolors.utils.FCUtils;

/**
 * @author bazleks on 1/31/2016.
 */
public class SquareRow extends Group {
    private ColorSchemeService colorSchemeService;
    private ColorType colorType;
    private String scoreStr;
    private float padding = Float.valueOf(20f);
    private boolean invert;

    public SquareRow(ColorType colorType, Integer score) {
        this.colorType = colorType;
        scoreStr = Integer.toString(score);
    }

    public void setInvert(boolean invert) {
        this.invert = invert;
    }

    public void setColorSchemeService(ColorSchemeService colorSchemeService) {
        this.colorSchemeService = colorSchemeService;
    }

    public void setPadding(float padding) {
        this.padding = padding;
    }

    @Override
    public void setBounds(float x, float y, float width, float height) {
        super.setBounds(x, y, width, height);
        Color color = colorSchemeService.getColor(colorType);

        float size = Math.min(width, height);

        Group squareContainer = new Group();
        Group labelContainer = new Group();

        SquareTexture squareTexture = new SquareTexture();
        squareTexture.setColor(color);
        squareTexture.setBounds(padding, padding, size - padding * 2, size - padding * 2);

        BitmapFont shadowedFont = FontFactory.getBitmapFont(FontFactory.SMALL_FONT);
        Label label = FCUtils.createLabel(shadowedFont, scoreStr);
//        label.setBounds(padding, padding, labelContainer.getWidth() - padding * 2, labelContainer.getHeight() - padding * 2);

        if (!invert) {
            label.setAlignment(Align.left);
            squareContainer.setBounds(0, 0, size, size);
            labelContainer.setBounds(squareContainer.getWidth(), 0, width - squareContainer.getWidth(), height);
        } else {
            label.setAlignment(Align.right);
            labelContainer.setBounds(0, 0, width - size, size);
            squareContainer.setBounds(labelContainer.getWidth(), 0, size, size);
        }
        label.setBounds(padding, padding, labelContainer.getWidth() - padding * 2, labelContainer.getHeight() - padding * 2);

        labelContainer.addActor(label);
        squareContainer.addActor(squareTexture);

        addActor(squareContainer);
        addActor(labelContainer);
    }
}
