package ru.justgo.fitcolors.screens.top;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import ru.justgo.fitcolors.common.ui.ShadowedLabel;
import ru.justgo.fitcolors.dto.ScoreResult;
import ru.justgo.fitcolors.enums.ColorType;
import ru.justgo.fitcolors.enums.GameType;
import ru.justgo.fitcolors.factories.FontFactory;
import ru.justgo.fitcolors.screens.BaseScreen;
import ru.justgo.fitcolors.services.Services;
import ru.justgo.fitcolors.services.ifaces.ColorSchemeService;
import ru.justgo.fitcolors.services.ifaces.NetworkService;
import ru.justgo.fitcolors.services.ifaces.ScreenSwitcherService;
import ru.justgo.fitcolors.utils.FCConstants;
import ru.justgo.fitcolors.utils.FCUtils;

import java.util.List;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;
import static ru.justgo.fitcolors.enums.GameType.*;

public class TopScreen extends BaseScreen {
    private static final String TOP_LABEL = "T O P";

    private ColorSchemeService colorSchemeService = Services.getService(ColorSchemeService.class);
    private ScreenSwitcherService screenSwitcherService = Services.getService(ScreenSwitcherService.class);
    private NetworkService networkService = Services.getService(NetworkService.class);

    @Override
    public Color getBkColor() {
        return FCConstants.mainScreenColor;
    }

    @Override
    public Actor getGUI() {
        Actor title = createTitle();

        Actor singleSwipeIcon = createGameTypeIcon(SINGLE_SWIPE);
        Actor multiSwipeIcon = createGameTypeIcon(MULTI_SWIPE);
        Actor touchIcon = createGameTypeIcon(TOUCH);

        // todo: create record rows

        Actor singleSwipeRecords = createGameTypeRecords(SINGLE_SWIPE);
        Actor multiSwipeRecords = createGameTypeRecords(MULTI_SWIPE);
        Actor touchRecords = createGameTypeRecords(TOUCH);

        Image homeButton = createHomeButton();

        Table root = new Table();
        root.setFillParent(true);

        root.add(title).colspan(3).expandX();
        root.row();
        root.add(singleSwipeIcon);
        root.add(multiSwipeIcon);
        root.add(touchIcon);
        root.row();
        root.add(singleSwipeRecords).expandY();
        root.add(multiSwipeRecords).expandY();
        root.add(touchRecords).expandY();
        root.row();
        root.add(homeButton).colspan(3).pad(5);

        return root;
    }

    private Actor createGameTypeRecords(final GameType gameType) {
        final Group records = new Group();
        final BitmapFont font = FontFactory.getBitmapFont(FontFactory.SMALL_FONT);
        final Label loadingLabel = FCUtils.createLabel(font, "?");
        loadingLabel.addAction(forever(sequence(alpha(0.5f, 1), alpha(1f, 1))));
        records.addActor(loadingLabel);

        new Thread(new Runnable() {
            @Override
            public void run() {
                List<ScoreResult> topList = networkService.getTopListForGameType(gameType);
                synchronized (TopScreen.this) {
                    final VerticalGroup verticalGroup = new VerticalGroup();
                    verticalGroup.setFillParent(true);
                    for (ScoreResult scoreResult : topList) {
                        String text = scoreResult.getUserName() + ": " + scoreResult.getScore();
                        final ShadowedLabel label = FCUtils.createShadowedLabel(font, text);
                        label.setColor(Color.BLACK);
                        Gdx.app.postRunnable(new Runnable() {
                            @Override
                            public void run() {
                                verticalGroup.addActor(label);
                            }
                        });
                    }
                    Gdx.app.postRunnable(new Runnable() {
                        @Override
                        public void run() {
                            records.removeActor(loadingLabel);
                            records.addActor(verticalGroup);
                        }
                    });
                }
            }
        }).start();


        return records;
    }

    private Image createHomeButton() {
        Image image = FCUtils.createImageFromTexture("results/home.png", 50, 50);
        image.addCaptureListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                screenSwitcherService.switchScreenBack();
            }
        });
        return image;
    }

    private Actor createGameTypeIcon(GameType gameType) {
        Actor gameTypeIcon = new Actor();
        gameTypeIcon.setSize(50, 50);
        return gameTypeIcon;

    }

    private Actor createTitle() {
        BitmapFont font = FontFactory.getBitmapFont(FontFactory.HUGE_FONT);
        ShadowedLabel label = new ShadowedLabel(font, TOP_LABEL);
        label.setColor(colorSchemeService.getColor(ColorType.A));
        return label;
    }
}
