package ru.justgo.fitcolors.screens.settings;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import ru.justgo.fitcolors.screens.BaseScreen;
import ru.justgo.fitcolors.services.ifaces.ColorSchemeService;
import ru.justgo.fitcolors.services.Services;
import ru.justgo.fitcolors.enums.ColorType;

/**
 * @author bazleks on 26.07.2015.
 */
public class SettingsScreen extends BaseScreen {
    @Override
    public Color getBkColor() {
        return Services.getService(ColorSchemeService.class).getColor(ColorType.C);
    }

    @Override
    public Actor getGUI() {
        return null;
    }
}
