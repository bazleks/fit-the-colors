package ru.justgo.fitcolors.screens.medals;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;

import ru.justgo.fitcolors.screens.BaseScreen;
import ru.justgo.fitcolors.enums.ColorType;
import ru.justgo.fitcolors.services.Services;
import ru.justgo.fitcolors.services.ifaces.ColorSchemeService;


public class MedalsScreen extends BaseScreen
{

	private ColorSchemeService colorSchemeService = Services.getService(ColorSchemeService.class);

	@Override
	public Color getBkColor()
	{
		return colorSchemeService.getColor(ColorType.B);
	}

	@Override
	public Actor getGUI()
	{
		return null;
	}
}
