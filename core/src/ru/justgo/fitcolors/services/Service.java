package ru.justgo.fitcolors.services;

/**
 * Interface for indicating class is a game service.
 * You can use {@link Services#getService(Class)} for getting any of game services
 */
public interface Service {
}
