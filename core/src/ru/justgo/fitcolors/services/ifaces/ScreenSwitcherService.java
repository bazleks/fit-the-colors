package ru.justgo.fitcolors.services.ifaces;

import ru.justgo.fitcolors.screens.BaseScreen;
import ru.justgo.fitcolors.services.Service;
import ru.justgo.fitcolors.utils.exceptions.NoScreenException;

public interface ScreenSwitcherService extends Service {

    @Deprecated
    void switchScreen(Class<? extends BaseScreen> screenClass, BaseScreen.ScreenIn in);

    @Deprecated
    void switchScreenBack(BaseScreen.ScreenIn in) throws NoScreenException;

    void switchScreenBack() throws NoScreenException;

    void switchScreen(Class<? extends BaseScreen> screenClass);
}
