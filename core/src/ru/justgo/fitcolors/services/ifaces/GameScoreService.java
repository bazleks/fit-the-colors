package ru.justgo.fitcolors.services.ifaces;

import ru.justgo.fitcolors.services.Service;
import ru.justgo.fitcolors.enums.ColorType;


/**
 * Every game score keeper
 */
public interface GameScoreService extends Service
{
	/**
	 * Increment score by certain color
	 *
	 * @param colorType
	 */
	void incrementScore(ColorType colorType);

	/**
	 * Reset all scores (removes all color types)
	 */
	void resetScore();

	/**
	 * Get current game score
	 *
	 * @return - score
	 */
	int getTotalScore();

	/**
	 * Get score by colorType
	 *
	 * @param colorType
	 * 		- color
	 * @return - current score by color type
	 */
	int getScoreByColor(ColorType colorType);

}
