package ru.justgo.fitcolors.services.ifaces;

import ru.justgo.fitcolors.services.Service;

import java.io.Serializable;

public interface StorageService extends Service {
    void save(String key, Serializable object);

    <T> T load(String key);
}
