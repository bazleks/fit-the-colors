package ru.justgo.fitcolors.services.ifaces;

import ru.justgo.fitcolors.services.Service;

/**
 * Service for defining square speed
 */
public interface SquareSpeedService extends Service {
    /**
     * Defines square speed by current score
     *
     * @param score - current user score
     * @return - speed
     */
    float squareSpeed(int score);
}
