package ru.justgo.fitcolors.services.ifaces;

import ru.justgo.fitcolors.dto.ScoreResult;
import ru.justgo.fitcolors.dto.User;
import ru.justgo.fitcolors.enums.GameType;
import ru.justgo.fitcolors.services.Service;

import java.util.List;


/**
 * Interface for working with network requests.
 * Every request is managed using current user
 */
public interface NetworkService extends Service {
    /**
     * Send the score of current user for played game mode
     *
     * @param score    - score amount
     * @param gameMode - game mode
     */
    void sendScore(Integer score, GameType gameMode);

    /**
     * Register a user (or get his profile) and store to {@see StoreService}
     */
    User registerCurrentUser();

    int getScoreByGameType(GameType gameMode);

    /**
     * Best player's place for selected game type
     *
     * @param gameType - game type
     * @return - place
     */
    Integer getBestPlaceForGameType(GameType gameType);

    List<ScoreResult> getTopListForGameType(GameType gameType);

    void getAchievements();

    /**
     * Get a place for current user of submitted score
     *
     * @param gameType - game type
     * @param score    - current score
     * @return - place at the top of all players
     */
    int getPlaceForScore(GameType gameType, int score);
}
