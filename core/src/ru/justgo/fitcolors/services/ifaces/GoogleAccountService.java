package ru.justgo.fitcolors.services.ifaces;

import ru.justgo.fitcolors.services.Service;


public interface GoogleAccountService extends Service {
    void login();

    String getId();

    String getFullName();
}
