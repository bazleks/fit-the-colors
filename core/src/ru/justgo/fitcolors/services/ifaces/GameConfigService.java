package ru.justgo.fitcolors.services.ifaces;

import ru.justgo.fitcolors.enums.GameType;
import ru.justgo.fitcolors.services.Service;

public interface GameConfigService extends Service {
    GameType getGameType();
}
