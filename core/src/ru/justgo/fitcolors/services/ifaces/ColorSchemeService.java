package ru.justgo.fitcolors.services.ifaces;

import java.util.List;

import com.badlogic.gdx.graphics.Color;

import ru.justgo.fitcolors.enums.ColorType;
import ru.justgo.fitcolors.services.Service;


public interface ColorSchemeService extends Service
{
	enum Complexity
	{
		EASY, MEDIUM, HARD
	}

	Color getColor(ColorType colorType);

	void switchColorScheme(Complexity complexity);

	Complexity getComplexity();

	List<Color> getColors(Complexity complexity);
}
