package ru.justgo.fitcolors.services.ifaces;

import ru.justgo.fitcolors.services.Service;

public interface ObjectsSizeCalculatorService extends Service {
    float getSquareSize();

    float getObstacleSize();
}
