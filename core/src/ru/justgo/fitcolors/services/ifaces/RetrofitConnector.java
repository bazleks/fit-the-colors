package ru.justgo.fitcolors.services.ifaces;

import retrofit2.Call;
import retrofit2.http.*;
import ru.justgo.fitcolors.dto.User;

import java.util.List;


public interface RetrofitConnector {
    class EmptyAnswer {

    }

    class ResultDefault {
        public List<ResultAnswer> result;
        public String error;
    }

    class ResultAnswer {
        public String userName;
        public int score;
        public String levelType;
    }

    class ScoresResultsWrapper {
        public ScoresResultField result;
    }

    class ScoresResultField {
        public Integer score;
    }

    class ScoreResult {
        public String userName;
        public String score;
        public String gameType;
    }

    class PositionResult {
        public String place;
    }

    @GET("top")
    Call<List<ScoreResult>> getTopResults(@Query("gameType") int gameType);

    @POST("score")
    @FormUrlEncoded
    Call<List<Object>> postResults(@Field("idUser") String userID, @Field("score") int score, @Field("gameType") int gameType);

    @POST("register")
    @FormUrlEncoded
    Call<List<User>> registerUser(@Field("idUser") String userID, @Field("name") String name);

    @GET("score")
    Call<ScoreResult> getScore(@Query("idUser") String userID, @Query("gameType") int gameType);

    @GET("place")
    Call<PositionResult> getPosition(@Query("idUser") String userID, @Query("gameType") int gameTypeCode);
}
