package ru.justgo.fitcolors.services;

import ru.justgo.fitcolors.FitColors;
import ru.justgo.fitcolors.services.ifaces.*;
import ru.justgo.fitcolors.services.impl.*;

import java.util.HashMap;
import java.util.Map;

public class Services {
    private static Map<Class<? extends Service>, Service> services;

    public static void initServices(FitColors fitColors) {
        services = new HashMap<>();
        services.put(ScreenSwitcherService.class, new ScreenSwitcherSwitcherServiceImpl(fitColors));
        services.put(ColorSchemeService.class, new ColorSchemeServiceImpl());
        services.put(GameScoreService.class, new GameScoreServiceImpl());
        services.put(ObjectsSizeCalculatorService.class, new ObjectsSizeCalculatorServiceImpl());
        services.put(SquareSpeedService.class, new SquareSpeedServiceImpl());
        services.put(StorageService.class, new StorageServiceImpl());
        services.put(GoogleAccountService.class, new GoogleAccountServiceImpl());
        services.put(NetworkService.class, new MockNetworkServiceImpl());
        services.put(GameConfigService.class, new GameConfigServiceImpl());
    }

    public static <T extends Service> T getService(Class<T> serviceClass) {
        return (T) services.get(serviceClass);
    }
}
