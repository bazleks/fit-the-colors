package ru.justgo.fitcolors.services.impl;

import com.badlogic.gdx.Gdx;
import ru.justgo.fitcolors.services.ifaces.SquareSpeedService;

public class SquareSpeedServiceImpl implements SquareSpeedService {

    @Override
    public float squareSpeed(int score) {
        float speed = Gdx.graphics.getHeight() / 3f;
        return speed + speed * (score / 100f);
    }
}
