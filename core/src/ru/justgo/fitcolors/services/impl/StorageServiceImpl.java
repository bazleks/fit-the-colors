package ru.justgo.fitcolors.services.impl;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import ru.justgo.fitcolors.services.ifaces.StorageService;

import java.io.Serializable;

public class StorageServiceImpl implements StorageService {

    public static final String PREFERENCES_KEY = "FIT_COLORS_PREFS";
    private Preferences preferences;

    public StorageServiceImpl() {
        preferences = Gdx.app.getPreferences(PREFERENCES_KEY);
    }

    @Override
    public void save(String key, Serializable object) {
        if (object instanceof String) {
            preferences.putString(key, (String) object);
            preferences.flush();

        }
    }

    @Override
    public <T> T load(String key) {
        return (T) preferences.getString(key);
    }
}
