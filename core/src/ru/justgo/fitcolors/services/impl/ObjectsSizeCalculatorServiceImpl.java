package ru.justgo.fitcolors.services.impl;

import com.badlogic.gdx.Gdx;
import ru.justgo.fitcolors.services.ifaces.ObjectsSizeCalculatorService;

/**
 * @author bazleks on 1/31/2016.
 */
public class ObjectsSizeCalculatorServiceImpl implements ObjectsSizeCalculatorService {

    private final static float SQUARE_COEF = 0.2f;
    private final static float OBSTACLE_COEF = 0.025f;

    @Override
    public float getSquareSize() {
        return Gdx.graphics.getHeight() * SQUARE_COEF;
    }

    @Override
    public float getObstacleSize() {
        return Gdx.graphics.getHeight() * OBSTACLE_COEF;
    }
}
