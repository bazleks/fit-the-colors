package ru.justgo.fitcolors.services.impl;

import ru.justgo.fitcolors.services.Services;
import ru.justgo.fitcolors.services.ifaces.ColorSchemeService;
import ru.justgo.fitcolors.services.ifaces.GameScoreService;
import ru.justgo.fitcolors.enums.ColorType;

import java.util.HashMap;
import java.util.Map;


public class GameScoreServiceImpl implements GameScoreService
{

	private ColorSchemeService colorSchemeService = Services.getService(ColorSchemeService.class);

	private Map<ColorType, Integer> scores = new HashMap<>();

	{
		scores.put(ColorType.A, 0);
		scores.put(ColorType.B, 0);
		scores.put(ColorType.C, 0);
		scores.put(ColorType.D, 0);
	}

	@Override
	public void incrementScore(ColorType colorType)
	{
		int scoreIncrement = getScoreIncrement();
		if (scores.containsKey(colorType))
		{
			Integer score = scores.get(colorType) + scoreIncrement;
			scores.put(colorType, score);
		}
		else
		{
			scores.put(colorType, scoreIncrement);
		}

		System.out.println(colorType + ": " + scores.get(colorType));
	}

	private int getScoreIncrement()
	{
		switch (colorSchemeService.getComplexity())
		{
			case EASY:
				return 1;
			case MEDIUM:
				return 2;
			case HARD:
				return 3;
			default:
				return 1;
		}
	}

	@Override
	public void resetScore()
	{
		scores.clear();
	}

	@Override
	public int getTotalScore()
	{
		int score = 0;
		for (Map.Entry<ColorType, Integer> colorTypeIntegerEntry : scores.entrySet())
		{
			score += colorTypeIntegerEntry.getValue();
		}
		return score;
	}

	@Override
	public int getScoreByColor(ColorType colorType)
	{
		return scores.containsKey(colorType) ? scores.get(colorType) : 0;
	}

}
