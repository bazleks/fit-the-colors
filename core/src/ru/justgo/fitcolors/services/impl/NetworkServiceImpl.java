package ru.justgo.fitcolors.services.impl;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.justgo.fitcolors.dto.ScoreResult;
import ru.justgo.fitcolors.dto.User;
import ru.justgo.fitcolors.enums.GameType;
import ru.justgo.fitcolors.services.Services;
import ru.justgo.fitcolors.services.ifaces.GoogleAccountService;
import ru.justgo.fitcolors.services.ifaces.NetworkService;
import ru.justgo.fitcolors.services.ifaces.RetrofitConnector;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class NetworkServiceImpl implements NetworkService {

    private static final String SERVER_HOST = "http://fitthecolors.000webhostapp.com";

    private RetrofitConnector connector;

    private GoogleAccountService googleAccountService = Services.getService(GoogleAccountService.class);

    public NetworkServiceImpl() {
        //@formatter:off
        Retrofit retrofit = new Retrofit.Builder().baseUrl(SERVER_HOST)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        //@formatter:on

        connector = retrofit.create(RetrofitConnector.class);
    }


    @Override
    public void sendScore(Integer score, GameType gameType) {
        try {
            String userId = googleAccountService.getId();
            int gameTypeCode = gameType.getCode();
            connector.postResults(userId, score, gameTypeCode).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public User registerCurrentUser() {
        try {
            String userId = googleAccountService.getId();
            String userName = googleAccountService.getFullName();
            return connector.registerUser(userId, userName).execute().body().get(0);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public int getScoreByGameType(GameType gameType) {
        System.out.println(String.format("NetworkServiceImpl#getScoreByGameType([%s])", gameType));
        try {
            String userId = googleAccountService.getId();
            String score = connector.getScore(userId, gameType.getCode()).execute().body().score;
            System.out.println(
                    String.format("NetworkServiceImpl#getScoreByGameType - got result: score = [%s] for gameType = [%s]", score,
                            gameType));
            return Integer.parseInt(score);
        } catch (IOException e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public Integer getBestPlaceForGameType(GameType gameType) {
        int gameTypeCode = gameType.getCode();

//		try
//		{
//			String place = connector.getPosition(CURRENT_USER_ID, gameTypeCode).execute().body().place;
//			if(place != null) {
//				return Integer.parseInt(place);
//			}
//			return -1;
//		}
//		catch (IOException e)
//		{
//			e.printStackTrace();
//			return null;
//		}
        return 0;
    }

    @Override
    public List<ScoreResult> getTopListForGameType(GameType gameType) {
        try {
            List<ScoreResult> results = new ArrayList<>();
            List<RetrofitConnector.ScoreResult> rawResult = connector.getTopResults(gameType.getCode()).execute().body();
            for (RetrofitConnector.ScoreResult result : rawResult) {
                ScoreResult scoreResult = createScoreResult(result);
                results.add(scoreResult);
            }
            return results;
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    public void getAchievements() {

    }

    @Override
    public int getPlaceForScore(GameType gameType, int score) {
        return 0;
    }

    private ScoreResult createScoreResult(RetrofitConnector.ScoreResult result) {
        ScoreResult scoreResult = new ScoreResult();
        scoreResult.setUserName(result.userName);
        scoreResult.setScore(Integer.parseInt(result.score));
        scoreResult.setGameType(GameType.findByCode(Integer.parseInt(result.gameType)));

        return scoreResult;
    }
}
