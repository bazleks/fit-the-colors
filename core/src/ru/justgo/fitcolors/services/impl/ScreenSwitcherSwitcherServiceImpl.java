package ru.justgo.fitcolors.services.impl;

import ru.justgo.fitcolors.FitColors;
import ru.justgo.fitcolors.screens.BaseScreen;
import ru.justgo.fitcolors.services.ifaces.ScreenSwitcherService;
import ru.justgo.fitcolors.utils.exceptions.NoScreenException;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;


public class ScreenSwitcherSwitcherServiceImpl implements ScreenSwitcherService {
    private Map<Class<? extends BaseScreen>, BaseScreen> screens = new HashMap<>();
    private Stack<Class<? extends BaseScreen>> screensStack = new Stack<>();

    private FitColors fitColors;

    public ScreenSwitcherSwitcherServiceImpl(FitColors fitColors) {
        this.fitColors = fitColors;
    }

    @Override
    @SuppressWarnings("unchecked")
    @Deprecated
    public void switchScreen(Class<? extends BaseScreen> screenClass, BaseScreen.ScreenIn in) {
        try {
            //TODO: screen animation transition
            final BaseScreen screen = screenClass.newInstance();
            screen.setScreenIn(in);
//			screen.setColorSchemeService(Services.getService(ColorSchemeService.class));
            final BaseScreen oldScreen = fitColors.getCurrentScreen();
            if (oldScreen != null) {
                oldScreen.hide();
                screensStack.push(oldScreen.getClass());
            }
            screen.show();
            fitColors.setCurrentScreen(screen);
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void switchScreenBack(BaseScreen.ScreenIn in) {
        final Class<? extends BaseScreen> prevScreen = screensStack.pop();
        switchScreen(prevScreen, in);
    }

    @Override
    public void switchScreenBack() throws NoScreenException {
        Class<? extends BaseScreen> previousScreen = screensStack.pop();
        switchScreen(previousScreen);
    }

    @Override
    public void switchScreen(Class<? extends BaseScreen> screenClass) {
        final BaseScreen nextScreen = getNextScreen(screenClass);
        final BaseScreen currentScreen = fitColors.getCurrentScreen();
        if (currentScreen != null) {
            currentScreen.hide();
            screensStack.push(currentScreen.getClass());

        }
        fitColors.setCurrentScreen(nextScreen);
        nextScreen.show();

    }

    private BaseScreen getNextScreen(Class<? extends BaseScreen> screenClass) {
        try {
            if (!screens.containsKey(screenClass)) {
                BaseScreen newScreen = screenClass.newInstance();
                screens.put(screenClass, newScreen);
            }
            return screens.get(screenClass);
        } catch (InstantiationException | IllegalAccessException e) {
            throw new NoScreenException(e);
        }


    }
}
