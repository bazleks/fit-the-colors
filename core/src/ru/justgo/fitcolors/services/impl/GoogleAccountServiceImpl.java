package ru.justgo.fitcolors.services.impl;

import ru.justgo.fitcolors.services.Services;
import ru.justgo.fitcolors.services.ifaces.GoogleAccountService;
import ru.justgo.fitcolors.services.ifaces.StorageService;

import java.util.UUID;

public class GoogleAccountServiceImpl implements GoogleAccountService {

    private static final String USER_ID = "USER_ID_KEY";
    private static final String USER_NAME = "USER_NAME_KEY";

    private StorageService storageService = Services.getService(StorageService.class);

    @Override
    public void login() {
        String userId = storageService.load(USER_ID);
        System.out.println("userId = " + userId);
        if (userId == null || userId.isEmpty()) {
            userId = UUID.randomUUID().toString();
            String userName = UUID.randomUUID().toString();
            storageService.save(USER_ID, userId);
            storageService.save(USER_NAME, userName);
        }
    }

    @Override
    public String getId() {
        return storageService.load(USER_ID);
    }

    @Override
    public String getFullName() {
        return storageService.load(USER_NAME);
    }
}
