package ru.justgo.fitcolors.services.impl;

import com.badlogic.gdx.math.MathUtils;
import ru.justgo.fitcolors.dto.ScoreResult;
import ru.justgo.fitcolors.dto.User;
import ru.justgo.fitcolors.enums.GameType;
import ru.justgo.fitcolors.services.ifaces.NetworkService;
import ru.justgo.fitcolors.utils.FCUtils;

import java.util.Arrays;
import java.util.List;


public class MockNetworkServiceImpl implements NetworkService {

    @Override
    public void sendScore(Integer score, GameType gameMode) {
        imitateDelay();
    }

    @Override
    public User registerCurrentUser() {
        imitateDelay();
        return new User();
    }

    @Override
    public int getScoreByGameType(GameType gameType) {
        imitateDelay();
        return 0;
    }

    @Override
    public Integer getBestPlaceForGameType(GameType gameType) {
        imitateDelay();
        return MathUtils.random(1, 10);
    }

    @Override
    public List<ScoreResult> getTopListForGameType(GameType gameType) {
        imitateDelay();
        return Arrays.asList(new ScoreResult(), new ScoreResult());
    }

    @Override
    public void getAchievements() {
        throw new IllegalStateException("Not implemented yet");
    }

    @Override
    public int getPlaceForScore(GameType gameType, int score) {
        imitateDelay();
        return 0;
    }

    private void imitateDelay() {
        FCUtils.sleep(2000);
    }
}
