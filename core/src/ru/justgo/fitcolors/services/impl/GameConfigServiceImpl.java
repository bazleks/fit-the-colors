package ru.justgo.fitcolors.services.impl;

import ru.justgo.fitcolors.enums.GameType;
import ru.justgo.fitcolors.services.ifaces.GameConfigService;

public class GameConfigServiceImpl implements GameConfigService {

    private GameType currentGameType;

    @Override
    public GameType getGameType() {
        return currentGameType;
    }

    public void setCurrentGameType(GameType gameType) {
        this.currentGameType = gameType;
    }
}
