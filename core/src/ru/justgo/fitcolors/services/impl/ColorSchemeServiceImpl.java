package ru.justgo.fitcolors.services.impl;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.badlogic.gdx.graphics.Color;

import ru.justgo.fitcolors.enums.ColorType;
import ru.justgo.fitcolors.services.ifaces.ColorSchemeService;


public class ColorSchemeServiceImpl implements ColorSchemeService
{
	private Map<Complexity, Map<ColorType, Color>> colorSchemes = new EnumMap<>(Complexity.class);
	private Complexity complexity;

	public ColorSchemeServiceImpl()
	{
		createColorScheme(Complexity.EASY, "eaa2b8", "78eabb", "dec1ff", "9f928c");
		createColorScheme(Complexity.MEDIUM, "aaa2b8", "bba2b8", "cca2b8", "dda2b8");
		createColorScheme(Complexity.HARD, "aaa2bb", "aaa7bb", "aaabbb", "aaaebb");

		switchColorScheme(Complexity.EASY);
	}

	private void createColorScheme(Complexity complexity, String colorA, String colorB, String colorC, String colorD)
	{
		Map<ColorType, Color> colorScheme = new EnumMap<>(ColorType.class);
		colorScheme.put(ColorType.A, Color.valueOf(colorA));
		colorScheme.put(ColorType.B, Color.valueOf(colorB));
		colorScheme.put(ColorType.C, Color.valueOf(colorC));
		colorScheme.put(ColorType.D, Color.valueOf(colorD));

		colorSchemes.put(complexity, colorScheme);
	}

	@Override
	public Color getColor(ColorType colorType)
	{
		return colorSchemes.get(complexity).get(colorType);
	}

	@Override
	public void switchColorScheme(Complexity complexity)
	{
		this.complexity = complexity;
	}

	@Override
	public Complexity getComplexity()
	{
		return complexity;
	}

	@Override
	public List<Color> getColors(Complexity complexity)
	{
		return new ArrayList<>(colorSchemes.get(complexity).values());
	}

}
