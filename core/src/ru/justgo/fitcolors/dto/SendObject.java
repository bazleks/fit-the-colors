package ru.justgo.fitcolors.dto;

import java.io.Serializable;

import ru.justgo.fitcolors.enums.GameType;

/**
 * Created by TssDragon
 */
public class SendObject implements Serializable{

    public GameType type_mode;
    public int score_type = 0;
    public Object obj;

    public SendObject(GameType t, int l, Object obj){
        this.type_mode = t;
        this.score_type = l; // 0 - global, 1 - friends, 2 - self score, 3 - registration, 4 - update_score
        this.obj = obj;
    }
}