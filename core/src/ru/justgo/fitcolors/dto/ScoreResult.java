package ru.justgo.fitcolors.dto;

import ru.justgo.fitcolors.enums.GameType;


public class ScoreResult
{
	private String userName;
	private Integer score;
	private GameType gameType;

	public String getUserName()
	{
		return userName;
	}

	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	public Integer getScore()
	{
		return score;
	}

	public void setScore(Integer score)
	{
		this.score = score;
	}

	public GameType getGameType()
	{
		return gameType;
	}

	public void setGameType(GameType gameType)
	{
		this.gameType = gameType;
	}
}
