package ru.justgo.fitcolors.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import ru.justgo.fitcolors.enums.GameType;

/**
 *
 * Created by TssDragon
 */
public class AnswerSelfScore implements Serializable {
    public int place_1;
    public int place_2;
    public int place_3;
    public Map<GameType, Integer> score;

    public AnswerSelfScore() {
        score = new HashMap<>();
    }

}
