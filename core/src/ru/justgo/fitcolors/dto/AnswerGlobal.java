package ru.justgo.fitcolors.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by TssDragon
 */
public class AnswerGlobal implements Serializable{
    public int self_place;
    public Map<String, Integer> leaders;

    public AnswerGlobal() {
        this.leaders = new HashMap<String, Integer>();
    }
}