package ru.justgo.fitcolors.dto;

import java.io.Serializable;

public class User implements Serializable {
    private String idUser;
    private String name;

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
