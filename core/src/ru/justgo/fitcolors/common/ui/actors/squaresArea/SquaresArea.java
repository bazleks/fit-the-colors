package ru.justgo.fitcolors.common.ui.actors.squaresArea;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;

import ru.justgo.fitcolors.common.ui.actors.Obstacle;
import ru.justgo.fitcolors.common.ui.actors.Square;
import ru.justgo.fitcolors.enums.ColorType;
import ru.justgo.fitcolors.services.Services;
import ru.justgo.fitcolors.services.ifaces.ObjectsSizeCalculatorService;
import ru.justgo.fitcolors.utils.collisions.CollisionListener;
import ru.justgo.fitcolors.utils.collisions.Collisions;
import ru.justgo.fitcolors.screens.game.controllers.squaresarea.UserController;

import java.util.ArrayList;
import java.util.List;


public abstract class SquaresArea extends Group
{
	private List<Square> squares = new ArrayList<>();
	private List<Obstacle> obstacles = new ArrayList<>();
	private CollisionListener collisionListener;
	protected UserController userController;

	private ObjectsSizeCalculatorService objectsSizeCalculatorService = Services.getService(ObjectsSizeCalculatorService.class);

	public void initialize()
	{
		System.out.println("SquaresArea#initialize");

		collisionListener = createCollisionListener();
		userController = createUserController();

		addListener(userController.getActorGestureListener());
	}

	@Override
	public void addActor(Actor actor)
	{
		super.addActor(actor);
		if (actor instanceof Square)
		{
			System.out.println("SquaresArea#addActor(Square)");
			squares.add((Square) actor);
		}
		else if (actor instanceof Obstacle)
		{
			System.out.println("SquaresArea#addActor(Obstacle)");
			obstacles.add((Obstacle) actor);
		}
	}

	@Override
	public void act(float delta)
	{
		super.act(delta);
		Collisions.checkForCollisions(obstacles, squares, collisionListener);
	}

	protected abstract CollisionListener createCollisionListener();

	protected abstract UserController createUserController();

	// region static create obstacles
	@Deprecated
	private Obstacle createHorizontalObstacle(ColorType colorType, float x, float y, float length)
	{
		float obstacleSize = objectsSizeCalculatorService.getObstacleSize();
		Obstacle obstacle = new Obstacle(colorType);
		obstacle.setBounds(x, y, length, obstacleSize);
		return obstacle;
	}

	@Deprecated
	private Obstacle createVerticalObstacle(ColorType colorType, float x, float y, float length)
	{
		float obstacleSize = objectsSizeCalculatorService.getObstacleSize();
		Obstacle obstacle = new Obstacle(colorType);
		obstacle.setBounds(x, y, obstacleSize, length);
		return obstacle;
	}

	@Deprecated
	protected Obstacle[] createDefaults(float width, float height)
	{
		float obstacleSize = objectsSizeCalculatorService.getObstacleSize();
		return new Obstacle[] { createHorizontalObstacle(ColorType.A, obstacleSize, height - obstacleSize,
				width - obstacleSize * 2), // top
				createVerticalObstacle(ColorType.B, width - obstacleSize, obstacleSize, height - obstacleSize * 2),
				// right
				createHorizontalObstacle(ColorType.C, obstacleSize, 0, width - obstacleSize * 2), // bottom
				createVerticalObstacle(ColorType.D, 0, obstacleSize, height - obstacleSize * 2), // left
		};
	}
	// endregion
}
