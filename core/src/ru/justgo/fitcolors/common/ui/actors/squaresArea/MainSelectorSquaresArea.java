package ru.justgo.fitcolors.common.ui.actors.squaresArea;

import java.util.HashMap;
import java.util.Map;

import ru.justgo.fitcolors.common.ui.SquareTexture;
import ru.justgo.fitcolors.common.ui.actors.Obstacle;
import ru.justgo.fitcolors.common.ui.actors.Square;
import ru.justgo.fitcolors.enums.ObstacleType;
import ru.justgo.fitcolors.screens.game.controllers.squaresarea.UserController;
import ru.justgo.fitcolors.screens.game.controllers.squaresarea.impl.SquaresAreaSwipeController;
import ru.justgo.fitcolors.screens.game.listeners.SwipeListener;
import ru.justgo.fitcolors.screens.main.MainTransitions;
import ru.justgo.fitcolors.screens.main.listeners.MainBoxSwipeListener;
import ru.justgo.fitcolors.utils.FCConstants;
import ru.justgo.fitcolors.utils.collisions.CollisionListener;


public class MainSelectorSquaresArea extends SquaresArea {

    private MainTransitions screenTransitions;
    private Map<ObstacleType, Obstacle> sidesMap;
    private Square userSquare;


    public MainSelectorSquaresArea(MainTransitions screenTransitions) {
        this.screenTransitions = screenTransitions;
    }

    @Override
    public void setBounds(float x, float y, float width, float height) {
        super.setBounds(x, y, width, height);

        final SquareTexture background = new SquareTexture();
        background.setBounds(0, 0, width, height);
        background.setColor(FCConstants.brownColor);
        addActor(background);

        sidesMap = createSides(width, height);
        addSidesToActor(sidesMap);

        int squareSize = (int) (height * 0.25f);

        userSquare = new Square();
        userSquare.setBounds((width - squareSize) / 2, (height - squareSize) / 2, squareSize, squareSize);
        addActor(userSquare);
    }

    private Map<ObstacleType, Obstacle> createSides(float width, float height) {
        Obstacle[] defaultSides = createDefaults(width, height);
        Map<ObstacleType, Obstacle> sidesMap = new HashMap<>();
        sidesMap.put(ObstacleType.MODES, defaultSides[0]);
        sidesMap.put(ObstacleType.MEDALS, defaultSides[1]);
        sidesMap.put(ObstacleType.SETTINGS, defaultSides[2]);
        sidesMap.put(ObstacleType.TOP, defaultSides[3]);
        return sidesMap;
    }

    private void addSidesToActor(Map<ObstacleType, Obstacle> sides) {
        for (Obstacle obstacle : sides.values()) {
            addActor(obstacle);
        }
    }

    @Override
    protected CollisionListener createCollisionListener() {
        return new CollisionListener() {
            @Override
            public void onSquareHitObstacle(Square square, Obstacle obstacle) {
                switch (obstacle.getColorType()) {
                    case A:
                        System.out.println("MainSelectorSquaresArea$CollisionListener#onSquareHitObstacle - PLAY");
                        screenTransitions.toModesScreenTransition();
                        break;
                    case B:
                        System.out.println("MainSelectorSquaresArea$CollisionListener#onSquareHitObstacle - MEDALS");
                        screenTransitions.toMedalsScreenTransition();
                        break;
                    case C:
                        System.out.println("MainSelectorSquaresArea$CollisionListener#onSquareHitObstacle - SETTINGS");
                        screenTransitions.toSettingsScreenTransition();
                        break;
                    case D:
                        System.out.println("MainSelectorSquaresArea$CollisionListener#onSquareHitObstacle - TOP");
                        screenTransitions.toTopScreenTransition();
                        break;
                }
            }
        };
    }

    @Override
    protected UserController createUserController() {
        SquaresAreaSwipeController swipeController = new SquaresAreaSwipeController();
        SwipeListener swipeListener = new MainBoxSwipeListener(sidesMap, userSquare);
        swipeController.setSwipeListener(swipeListener);
        this.addListener(swipeController.getActorGestureListener());
        return swipeController;
    }
}