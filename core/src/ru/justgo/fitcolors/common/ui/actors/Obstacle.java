package ru.justgo.fitcolors.common.ui.actors;

import com.badlogic.gdx.graphics.Color;

import ru.justgo.fitcolors.common.ui.SquareTexture;
import ru.justgo.fitcolors.enums.ColorType;
import ru.justgo.fitcolors.services.Services;
import ru.justgo.fitcolors.services.ifaces.ColorSchemeService;


public class Obstacle extends SquareTexture
{
	private ColorSchemeService colorSchemeService = Services.getService(ColorSchemeService.class);
	private ColorType colorType;

	public Obstacle(ColorType colorType)
	{
		this.colorType = colorType;
		setColorType(colorType);
	}

	public void setColorType(ColorType colorType)
	{
		this.colorType = colorType;
		setColorInternal(colorType);
	}

	private void setColorInternal(ColorType colorType)
	{
		Color color = colorSchemeService.getColor(colorType);
		this.setColor(color);
	}

	public ColorType getColorType()
	{
		return colorType;
	}
}
