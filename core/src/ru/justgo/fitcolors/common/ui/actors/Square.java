package ru.justgo.fitcolors.common.ui.actors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;

import ru.justgo.fitcolors.common.ui.SquareTexture;
import ru.justgo.fitcolors.enums.ColorType;
import ru.justgo.fitcolors.services.Services;
import ru.justgo.fitcolors.services.ifaces.ColorSchemeService;


public class Square extends SquareTexture
{
	private ColorSchemeService colorSchemeService = Services.getService(ColorSchemeService.class);

	private Vector2 velocity = new Vector2();
	private ColorType colorType;


	@Override
	public void act(float delta)
	{
		setX(getX() + velocity.x * delta);
		setY(getY() + velocity.y * delta);
		super.act(delta);
	}

	public ColorType getColorType()
	{
		return colorType;
	}

	public void setColorType(ColorType colorType)
	{
		this.colorType = colorType;
		Color color = colorSchemeService.getColor(colorType);
		this.setColor(color);
	}

	public void setVelocity(Vector2 velocity)
	{
		this.velocity = velocity;
	}

	public Vector2 getVelocity()
	{
		return velocity;
	}

	public void accelerateSquare()
	{
		//todo: implement acceleration
	}
}
