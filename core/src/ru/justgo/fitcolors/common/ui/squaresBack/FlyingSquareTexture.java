package ru.justgo.fitcolors.common.ui.squaresBack;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

import ru.justgo.fitcolors.enums.VelocityVector;
import ru.justgo.fitcolors.utils.FCUtils;
import ru.justgo.fitcolors.common.ui.SquareTexture;
import ru.justgo.fitcolors.common.ui.squaresBack.strategies.FlyingSquareColorStrategy;
import ru.justgo.fitcolors.enums.ColorType;


/**
 * @author bazleks on 26.07.2015.
 */
public class FlyingSquareTexture extends SquareTexture
{

	private final FlyingSquaresArea flyingSquaresArea;
	private Vector2 velocity;

	public FlyingSquareTexture(FlyingSquaresArea flyingSquaresArea)
	{
		this.flyingSquaresArea = flyingSquaresArea;
	}


	@Override
	public void act(float delta)
	{
		super.act(delta);
		float x = getX() + velocity.x * delta;
		float y = getY() + velocity.y * delta;
		boolean outOfBounds = x < -getWidth() || x > Gdx.graphics.getWidth() || y < -getHeight() || y > Gdx.graphics.getHeight();
		if (outOfBounds)
		{
			regenerate();
		}
		else
		{
			setPosition(x, y);
		}
	}

	public void regenerate()
	{
		float sizeMin = flyingSquaresArea.getHeight() * 0.05f;
		float sizeMax = flyingSquaresArea.getHeight() * 0.1f;
		float size = MathUtils.random(sizeMin, sizeMax);
		float x = MathUtils.random(0, flyingSquaresArea.getWidth() - size);
		float y = MathUtils.random(0, flyingSquaresArea.getHeight() - size);

		float speed = size;

		VelocityVector velocityVector = FCUtils.getRandFromArray(VelocityVector.values());
		velocity = velocityVector.getVelocity().cpy().scl(speed);

		this.setBounds(x, y, size, size);
		setSquareColor();
		this.getColor().a = 0f;
		this.addAction(Actions.alpha(0.75f, 1f));
	}

	private void setSquareColor()
	{
		FlyingSquareColorStrategy squareColorStrategy = flyingSquaresArea.getSquareColorStrategy();
		Color color = squareColorStrategy.getColor(FCUtils.getRandFromArray(ColorType.values()));
		this.setColor(color);
	}
}
