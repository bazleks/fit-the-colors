package ru.justgo.fitcolors.common.ui.squaresBack.strategies.impl;

import com.badlogic.gdx.graphics.Color;

import ru.justgo.fitcolors.services.ifaces.ColorSchemeService;
import ru.justgo.fitcolors.common.ui.squaresBack.strategies.FlyingSquareColorStrategy;
import ru.justgo.fitcolors.enums.ColorType;


public class MainScreenSquareColorStrategy implements FlyingSquareColorStrategy
{
	private ColorSchemeService colorSchemeService;

	public MainScreenSquareColorStrategy(ColorSchemeService colorSchemeService)
	{
		this.colorSchemeService = colorSchemeService;
	}

	@Override
	public Color getColor(ColorType square)
	{
		return colorSchemeService.getColor(square);
	}
}
