package ru.justgo.fitcolors.common.ui.squaresBack.strategies.impl;

import com.badlogic.gdx.graphics.Color;

import ru.justgo.fitcolors.common.ui.squaresBack.strategies.FlyingSquareColorStrategy;
import ru.justgo.fitcolors.enums.ColorType;


public class ModesScreenSquareColorStrategy implements FlyingSquareColorStrategy
{
	private static final Color DEFAULT_COLOR = new Color(0, 0, 0, 0.25f);

	@Override
	public Color getColor(ColorType colorType)
	{
		return DEFAULT_COLOR;
	}
}
