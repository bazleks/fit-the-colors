package ru.justgo.fitcolors.common.ui.squaresBack.strategies;

import com.badlogic.gdx.graphics.Color;

import ru.justgo.fitcolors.enums.ColorType;


public interface FlyingSquareColorStrategy
{
	Color getColor(ColorType square);
}
