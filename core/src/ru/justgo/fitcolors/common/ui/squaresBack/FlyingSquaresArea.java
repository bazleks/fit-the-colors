package ru.justgo.fitcolors.common.ui.squaresBack;

import com.badlogic.gdx.scenes.scene2d.Group;

import ru.justgo.fitcolors.services.Services;
import ru.justgo.fitcolors.services.ifaces.ColorSchemeService;
import ru.justgo.fitcolors.common.ui.squaresBack.strategies.FlyingSquareColorStrategy;


public class FlyingSquaresArea extends Group
{

	private final int count;
	private FlyingSquareColorStrategy squareColorStrategy;

	private ColorSchemeService colorSchemeService = Services.getService(ColorSchemeService.class);

	public FlyingSquaresArea(int count, FlyingSquareColorStrategy colorStrategy)
	{
		this.count = count;
		this.squareColorStrategy = colorStrategy;
	}

	@Override
	public void setBounds(float x, float y, float width, float height)
	{
		super.setBounds(x, y, width, height);
		this.clear(); // clear children
		createSquares();
	}

	private void createSquares()
	{
		for (int i = 0; i < count; i++)
		{
			FlyingSquareTexture flyingSquare = new FlyingSquareTexture(this);
			flyingSquare.regenerate();
			this.addActor(flyingSquare);
		}
	}

	public FlyingSquareColorStrategy getSquareColorStrategy()
	{
		return squareColorStrategy;
	}

	public void setSquareColorStrategy(FlyingSquareColorStrategy squareColorStrategy)
	{
		this.squareColorStrategy = squareColorStrategy;
	}
}
