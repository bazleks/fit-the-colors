package ru.justgo.fitcolors.common.ui;

import com.badlogic.gdx.graphics.Color;
import ru.justgo.fitcolors.services.Services;
import ru.justgo.fitcolors.services.ifaces.ColorSchemeService;
import ru.justgo.fitcolors.enums.ColorType;

public class ColorTypedSquare extends SquareTexture {

    private ColorType colorType;

    public ColorTypedSquare(ColorType colorType) {
        this.colorType = colorType;
    }

    @Override
    public Color getColor() {
        return Services.getService(ColorSchemeService.class).getColor(colorType);
    }

    public ColorType getColorType() {
        return colorType;
    }
}
