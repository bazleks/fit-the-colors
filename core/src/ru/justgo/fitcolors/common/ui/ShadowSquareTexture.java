package ru.justgo.fitcolors.common.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Group;


public class ShadowSquareTexture extends Group
{
	private SquareTexture square;
	private SquareTexture shadow;

	public ShadowSquareTexture(Color color)
	{
		square = new SquareTexture();
		square.setColor(color);

		shadow = new SquareTexture();
		shadow.setColor(new Color(0, 0, 0, 0.25f));

		addActor(shadow);
		addActor(square);
	}

	@Override
	public void setBounds(float x, float y, float width, float height)
	{
		square.setBounds(0, 0, width, height);
		shadow.setBounds(width * 0.1f, -height * 0.1f, width, height);
		super.setBounds(x, y, width, height);
	}
}
