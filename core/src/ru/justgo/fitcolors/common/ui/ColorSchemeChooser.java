package ru.justgo.fitcolors.common.ui;

import java.util.List;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;

import ru.justgo.fitcolors.enums.ColorChooserDirection;
import ru.justgo.fitcolors.services.Services;
import ru.justgo.fitcolors.services.ifaces.ColorSchemeService;


public class ColorSchemeChooser extends ColorChooser
{

	private static final int SQUARE_SIZE = 25;
	public static final float DURATION = 0.25f;

	private ColorSchemeService colorSchemeService = Services.getService(ColorSchemeService.class);

	ColorSchemeItem selectedColorScheme;

	ColorSchemeItem easyColorScheme;
	ColorSchemeItem mediumColorScheme;
	ColorSchemeItem hardColorScheme;

	@Override
	public void open(ColorChooserDirection direction, int x, int y)
	{
		easyColorScheme.addAction(Actions.moveTo(0, SQUARE_SIZE * 2, DURATION));
		mediumColorScheme.addAction(Actions.moveTo(0, SQUARE_SIZE * 4, DURATION));
		hardColorScheme.addAction(Actions.moveTo(0, SQUARE_SIZE * 6, DURATION));
//		setItemsVisible(true);
	}

	@Override
	public void close()
	{
		easyColorScheme.addAction(Actions.moveTo(0, 0, DURATION));
		mediumColorScheme.addAction(Actions.moveTo(0, 0, DURATION));
		hardColorScheme.addAction(Actions.moveTo(0, 0, DURATION));
//		setItemsVisible(false);
	}

	private void setItemsVisible(boolean visible)
	{
		easyColorScheme.setVisible(visible);
		mediumColorScheme.setVisible(visible);
		hardColorScheme.setVisible(visible);
	}

	@Override
	public Group getGUI()
	{
		Group group = new Group();
		group.setBounds(0, 0, SQUARE_SIZE * 2, SQUARE_SIZE * 2 * 4);


		final ColorSchemeService.Complexity currentComplexity = colorSchemeService.getComplexity();
		selectedColorScheme = new ColorSchemeItem(currentComplexity);
		addListener(new ActorGestureListener()
		{
			@Override
			public void touchDown(InputEvent event, float x, float y, int pointer, int button)
			{
				ColorSchemeChooser.this.open(null, 0, 0);
			}

			@Override
			public void touchUp(InputEvent event, float x, float y, int pointer, int button)
			{
				changeColorScheme(x, y);
				ColorSchemeChooser.this.close();
			}

			private void changeColorScheme(float x, float y)
			{
				Actor colorSchemeGroup = hit(x, y, true);
				if (colorSchemeGroup instanceof ColorSchemeItem)
				{
					ColorSchemeService.Complexity complexity = ((ColorSchemeItem) colorSchemeGroup).complexity;
					colorSchemeService.switchColorScheme(complexity);
					// todo: update screen
					removeActor(selectedColorScheme);
					selectedColorScheme = new ColorSchemeItem(complexity);
					addActor(selectedColorScheme);
				}
			}
		});

		easyColorScheme = new ColorSchemeItem(ColorSchemeService.Complexity.EASY);
		mediumColorScheme = new ColorSchemeItem(ColorSchemeService.Complexity.MEDIUM);
		hardColorScheme = new ColorSchemeItem(ColorSchemeService.Complexity.HARD);

		easyColorScheme.setPosition(0, SQUARE_SIZE * 2);
		mediumColorScheme.setPosition(0, SQUARE_SIZE * 4);
		hardColorScheme.setPosition(0, SQUARE_SIZE * 6);

		group.addActor(selectedColorScheme);
		group.addActor(easyColorScheme);
		group.addActor(mediumColorScheme);
		group.addActor(hardColorScheme);

		return group;
	}

	private static class ColorSchemeItem extends Group
	{
		private ColorSchemeService.Complexity complexity;
		private ColorSchemeService colorSchemeService = Services.getService(ColorSchemeService.class);

		public ColorSchemeItem(final ColorSchemeService.Complexity complexity)
		{
			this.complexity = complexity;

			initialize(complexity);
		}

		private void initialize(ColorSchemeService.Complexity complexity)
		{
			setSize(SQUARE_SIZE * 2, SQUARE_SIZE * 2);
			List<Color> colors = colorSchemeService.getColors(complexity);
			addActor(createSquare(colors.get(0), 0, 0));
			addActor(createSquare(colors.get(1), 0, SQUARE_SIZE));
			addActor(createSquare(colors.get(2), SQUARE_SIZE, 0));
			addActor(createSquare(colors.get(3), SQUARE_SIZE, SQUARE_SIZE));
		}

		private Actor createSquare(Color color, int x, int y)
		{
			SquareTexture square = new SquareTexture();
			square.setTouchable(Touchable.disabled);
			square.setColor(color);
			square.setBounds(x, y, SQUARE_SIZE, SQUARE_SIZE);

			return square;
		}
	}
}
