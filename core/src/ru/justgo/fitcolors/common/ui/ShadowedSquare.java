package ru.justgo.fitcolors.common.ui;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;

import ru.justgo.fitcolors.utils.FCUtils;


public class ShadowedSquare extends Group
{
	private SquareTexture square;
	private Actor shadow;

	@Override
	public void setBounds(float x, float y, float width, float height)
	{
		super.setBounds(x, y, width, height);

		shadow = FCUtils.createShadowTexture(x, y, (float) (width * Math.sqrt(2)));
		square = new SquareTexture();
		square.setBounds(0, 0, width, height);

		addActor(shadow);
		addActor(square);
	}

	@Override
	public void draw(Batch batch, float parentAlpha)
	{
		shadow.draw(batch, parentAlpha);
		super.draw(batch, parentAlpha);
	}
}
