package ru.justgo.fitcolors.common.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Label;

import ru.justgo.fitcolors.utils.FCUtils;


@Deprecated
public class ShadowedLabel extends Group
{
	private Label mainLabel;
	private Label shadow;

	public ShadowedLabel(BitmapFont bitmapFont, String text)
	{
		mainLabel = FCUtils.createLabel(bitmapFont, text);
		shadow = FCUtils.createLabel(bitmapFont, text);
		float shadowOffset = /*Math.min(mainLabel.getWidth() * 0.07f, mainLabel.getHeight() * 0.07f);*/2;
		shadow.setPosition(shadowOffset, -shadowOffset);
		shadow.setColor(0, 0, 0, 0.25f);
		addActor(shadow);
		addActor(mainLabel);

		setSize(mainLabel.getWidth(), mainLabel.getHeight());

	}

	@Override
	public void setWidth(float width)
	{
		super.setWidth(width);
		mainLabel.setWidth(getWidth());
		shadow.setWidth(getWidth());
	}

	public void setText(String text)
	{
		mainLabel.setText(text);
		shadow.setText(text);
	}

	@Override
	public void setColor(Color color)
	{
		mainLabel.setColor(color);
		super.setColor(color);
	}
}
