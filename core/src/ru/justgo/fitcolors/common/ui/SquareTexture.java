package ru.justgo.fitcolors.common.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;

import ru.justgo.fitcolors.utils.FCUtils;


public class SquareTexture extends Actor
{
	private TextureRegion textureRegion;

	public SquareTexture()
	{
		Texture whiteTexture = FCUtils.createWhiteTexture();
		textureRegion = new TextureRegion(whiteTexture);
	}

	@Override
	public void setBounds(float x, float y, float width, float height)
	{
		super.setBounds(x, y, width, height);
		setTextureRegionSize((int) width, (int) height);
	}

	@Override
	public void setSize(float width, float height)
	{
		super.setSize(width, height);
		setTextureRegionSize((int) width, (int) height);
	}

	@Override
	public void draw(Batch batch, float parentAlpha)
	{
		Color oldColor = batch.getColor();
		batch.setColor(getColor());
		if (textureRegion != null)
		{
			batch.draw(textureRegion, getX(), getY(), getOriginX(), getOriginY(), textureRegion.getRegionWidth(),
					textureRegion.getRegionHeight(), getScaleX(), getScaleY(), getRotation());
		}
		batch.setColor(oldColor);
		super.draw(batch, parentAlpha);
	}

	private void setTextureRegionSize(int width, int height)
	{
		textureRegion.setRegionWidth(width);
		textureRegion.setRegionHeight(height);
	}
}
