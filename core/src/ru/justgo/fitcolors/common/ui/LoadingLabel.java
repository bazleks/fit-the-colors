package ru.justgo.fitcolors.common.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;

import ru.justgo.fitcolors.common.ui.actors.Square;
import ru.justgo.fitcolors.utils.FCConstants;


public class LoadingLabel extends Group
{
//	private Label label;
//	private Square loading;
//	private LabelLabelLoaderListener labelLoaderListener;
//
//	public LoadingLabel(Label label)
//	{
//		this.label = label;
//	}
//
//	@Override
//	public void setBounds(float x, float y, float width, float height)
//	{
//		super.setBounds(x, y, width, height);
//
//		float size = Math.min(width, height) * 0.7f;
//
//		loading = new Square();
//		loading.setPosition((width - size) / 2, (height - size) / 2);
//		loading.setSize(size, size);
//		loading.setOrigin(Align.center);
//		loading.setColor(FCConstants.recordUsualColorTransparent);
//		loading.addAction(Actions.forever(Actions.rotateBy(90, 1f, Interpolation.exp5)));
//
//		label.setBounds(0, 0, width, height);
//		label.setOrigin(Align.center);
//		label.setColor(FCConstants.recordUsualColor);
//		label.getColor().a = 0f;
//
//		addActor(loading);
//		addActor(label);
//
//		new Thread(new Runnable()
//		{
//			@Override
//			public void run()
//			{
//				final String loadedValue = labelLoaderListener.load();
//				Gdx.app.postRunnable(new Runnable()
//				{
//					@Override
//					public void run()
//					{
//						labelLoaderListener.show(loadedValue);
//					}
//				});
//			}
//		}).start();
//	}
//
//	public void setText(String text)
//	{
//		label.setText(text);
//		loading.addAction(Actions.sequence(Actions.alpha(0f, 1f), Actions.hide()));
//		label.addAction(Actions.alpha(1f, 1f));
//	}
//
//	public void setLabelLoaderTextProvider(LabelLabelLoaderListener labelLoaderListener)
//	{
//		this.labelLoaderListener = labelLoaderListener;
//	}
//
//
//	public abstract class LabelLabelLoaderListenerImpl implements LabelLabelLoaderListener
//	{
//		@Override
//		public void show(String loadedValue)
//		{
//			setText(loadedValue);
//		}
//	}
}
