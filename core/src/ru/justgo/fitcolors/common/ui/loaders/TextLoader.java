package ru.justgo.fitcolors.common.ui.loaders;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.utils.Align;

import ru.justgo.fitcolors.common.ui.SquareTexture;
import ru.justgo.fitcolors.utils.FCConstants;


public abstract class TextLoader extends Group
{
	private SquareTexture loading;

	protected void addLoadingSquare() {
		System.out.println("LOADING SQUARE");
		float width = getWidth();
		float height = getHeight();
		System.out.println("width = " + width);
		System.out.println("height = " + height);
		float size = Math.min(width, height) * 0.7f;

		System.out.println("size = " + size);

		loading = new SquareTexture();
		loading.setPosition((width - size) / 2, (height - size) / 2);
		loading.setSize(size, size);
		loading.setOrigin(Align.center);
		loading.setColor(FCConstants.recordUsualColorTransparent);
		loading.addAction(Actions.forever(Actions.rotateBy(90, 1f, Interpolation.exp5)));

		addActor(loading);
	}

	public abstract String loadText();
}
