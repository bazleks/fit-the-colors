package ru.justgo.fitcolors.common.ui.loaders;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

import ru.justgo.fitcolors.common.ui.ShadowedLabel;
import ru.justgo.fitcolors.factories.FontFactory;
import ru.justgo.fitcolors.utils.FCUtils;


@Deprecated
public abstract class ShadowedLabelLoader extends TextLoader
{
	private ShadowedLabel shadowedLabel;

	public ShadowedLabelLoader()
	{
		BitmapFont font = FontFactory.getBitmapFont(FontFactory.SMALL_FONT);
		shadowedLabel = FCUtils.createShadowedLabel(font);
		startLoading();
		addActor(shadowedLabel);
		System.out.println("WIDTH = " + shadowedLabel.getWidth());
		addLoadingSquare();
	}

	private void startLoading()
	{
		new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				final String text = ShadowedLabelLoader.this.loadText();
				Gdx.app.postRunnable(new Runnable()
				{
					@Override
					public void run()
					{
						shadowedLabel.setText(text);
					}
				});
			}
		}).start();
	}
}
