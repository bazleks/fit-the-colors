package ru.justgo.fitcolors.common.ui.loaders;



import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.utils.Align;

import ru.justgo.fitcolors.utils.FCConstants;
import ru.justgo.fitcolors.utils.FCUtils;


public class LabelLoader extends Stack
{
	private Label label;
	private Label loading;
	private LabelLoaderTextProvider labelLoaderTextProvider;

	public LabelLoader(BitmapFont bitmapFont)
	{
		this.label = FCUtils.createLabel(bitmapFont);
		this.loading = FCUtils.createLabel(bitmapFont, "?");

		addActor(loading);
		addActor(label);
	}

	public void update()
	{
		loadOnSeparateThread();
	}

	@Override
	public void setBounds(float x, float y, float width, float height)
	{
		super.setBounds(x, y, width, height);

		loading.setOrigin(Align.center);
		loading.setColor(FCConstants.recordUsualColorTransparent);
		loading.addAction(Actions.forever(Actions.sequence(Actions.alpha(0.5f, 1f), Actions.alpha(1f, 1f))));

		label.setOrigin(Align.center);
		label.setColor(FCConstants.recordUsualColor);
		label.getColor().a = 0f;
	}

	private void loadOnSeparateThread()
	{
		System.out.println("UPDATE");
		new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				final String loadedValue = labelLoaderTextProvider.provide();
				Gdx.app.postRunnable(new Runnable()
				{
					@Override
					public void run()
					{
						setText(loadedValue);
					}
				});
			}
		}).start();
	}

	private void setText(String text)
	{
		label.setText(text);
		loading.clearActions();
		loading.addAction(Actions.sequence(Actions.alpha(0f, 1f), Actions.hide()));
		label.addAction(Actions.alpha(1f, 1f));
	}

	public void setLabelLoaderTextProvider(LabelLoaderTextProvider labelLoaderTextProvider)
	{
		this.labelLoaderTextProvider = labelLoaderTextProvider;
	}
}
