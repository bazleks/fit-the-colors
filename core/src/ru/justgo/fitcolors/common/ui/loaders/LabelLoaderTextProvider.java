package ru.justgo.fitcolors.common.ui.loaders;

public interface LabelLoaderTextProvider
{
	String provide();
}
