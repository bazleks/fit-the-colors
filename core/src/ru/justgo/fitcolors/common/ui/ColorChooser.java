package ru.justgo.fitcolors.common.ui;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;

import ru.justgo.fitcolors.enums.ColorChooserDirection;
import ru.justgo.fitcolors.services.Services;
import ru.justgo.fitcolors.services.ifaces.ColorSchemeService;
import ru.justgo.fitcolors.common.ui.actors.Obstacle;
import ru.justgo.fitcolors.enums.ColorType;


public class ColorChooser extends Group
{

	private Obstacle selectedObstacle;
	private ColorType selectedColorType;

	private ColorSchemeService colorSchemeService = Services.getService(ColorSchemeService.class);

	public void initialize()
	{
		Group gui = getGUI();
		addActor(gui);
	}


	public Group getGUI()
	{
		HorizontalGroup group = new HorizontalGroup();
		ColorType[] colors = ColorType.values();
		for (int i = 0; i < colors.length; i++)
		{
			SquareTexture square = createSquare(colors[i]);
			group.addActor(square);
		}
		return group;
	}

	public void open(ColorChooserDirection direction, int x, int y)
	{
		setVisible(true);
		switch (direction)
		{
			case TO_UP:
				setRotation(90);
				break;
			case TO_DOWN:
				setRotation(-90);
				break;
			case TO_LEFT:
				setRotation(180);
				break;
			case TO_RIGHT:
				setRotation(0);
				break;
		}
		// set visible
		// set coordinate to mouse x
		// turn by direction
		// open animation
	}

	public void close()
	{
		setVisible(false);
	}

	private SquareTexture createSquare(ColorType colorType)
	{
		SquareTexture squareTexture = new ColorTypedSquare(colorType);
		squareTexture.setSize(30, 30);//todo: magic numbers
		return squareTexture;
	}

	public void changeObstacleColor()
	{
		if (selectedObstacle != null && selectedColorType != null)
		{
			selectedObstacle.setColorType(selectedColorType);
		}
	}

	public ColorType getSelectedColorType()
	{
		return selectedColorType;
	}

	public void setSelectedColorType(ColorType selectedColorType)
	{
		this.selectedColorType = selectedColorType;
	}

	public Obstacle getSelectedObstacle()
	{
		return selectedObstacle;
	}

	public void setSelectedObstacle(Obstacle selectedObstacle)
	{
		this.selectedObstacle = selectedObstacle;
	}
}
