package ru.justgo.fitcolors.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class ApplicationConfigurationBuilder {

    private static final int PX960 = 960;
    private static final int PX540 = 540;

    private LwjglApplicationConfiguration config;

    public ApplicationConfigurationBuilder() {
        config = new LwjglApplicationConfiguration();
    }

    public ApplicationConfigurationBuilder horizontalView() {
        config.width = PX960;
        config.height = PX540;
        return this;
    }

    public ApplicationConfigurationBuilder verticalView() {
        config.width = PX540;
        config.height = PX960;
        return this;
    }

    public LwjglApplicationConfiguration build() {
        return config;
    }
}
