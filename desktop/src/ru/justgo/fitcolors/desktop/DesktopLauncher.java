package ru.justgo.fitcolors.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import ru.justgo.fitcolors.FitColors;

public class DesktopLauncher {

    public static void main(String[] arg) {
        LwjglApplicationConfiguration configuration = new ApplicationConfigurationBuilder()
                .verticalView()
                .build();

        FitColors applicationListener = new FitColors();

        new LwjglApplication(applicationListener, configuration);
    }
}
